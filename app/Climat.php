<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Climat extends Model
{
    protected $table = "CLIMAT_MONTH";

    public function station() {
        return $this->belongsTo('App\Station', 'IND_ST');
    }

    public function filterClimat(){
        $climat = DB::table('CLIMAT_MONTH')
            ->join('CAT_OBL', 'CAT_STATION.OBL_ID', '=', 'CAT_OBL.OBL_ID')
            ->select('CAT_STATION.IND_ST', 'CAT_STATION.NAME_ST')
            ->orderBy('CAT_STATION.OBL_ID', 'asc')
            ->orderBy('CAT_STATION.IND_ST')
            ->get();

        return $climat;
    }
}

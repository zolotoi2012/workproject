<?php


namespace App\Exports;

use DB;
use Auth;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use App\UserCategory;
use App\Helpers\{
    Helper
};

/**
 * Class KndailyExport
 * @package App\Exports
 */
class DateForMonthExport implements FromView, ShouldAutoSize
{
    /**
     * @var array
     */
    private $categories = [
        [
            'col_name' => 'Назва області',
            'short_col_name' => 'Назва області',
            'code_col_name' => 'NAME_OBL'
        ],
        [
            'col_name' => 'Назва станції',
            'short_col_name' => 'Назва станції',
            'code_col_name' => 'NAME_ST'
        ],
        [
            'col_name' => 'Індекс',
            'short_col_name' => 'Індекс',
            'code_col_name' => 'IND_ST'
        ],
        [
            'col_name' => 'Рік',
            'short_col_name' => 'Рік',
            'code_col_name' => 'YEAR_CH'
        ],
        [
            'col_name' => 'Місяць',
            'short_col_name' => 'Місяць',
            'code_col_name' => 'MONTH_CH'
        ],
        [
            'col_name' => 'Середня Т',
            'short_col_name' => 'Середня Т',
            'code_col_name' => 'T'
        ],
        [
            'col_name' => 'Сума опадів',
            'short_col_name' => 'Сума опадів',
            'code_col_name' => 'OS'
        ],
    ];

    private $collumns = ['NAME_OBL', 'NAME_ST', 'IND_ST', 'YEAR_CH', 'MONTH_CH', 'T', 'OS'];

    /**
     * @var array
     */
    private $data = [];

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @return View
     */
    public function view(): View
    {
        $helper = new Helper();

        $uId = Auth::getUser()->getAuthIdentifier();

        if (DB::table('user_categories')->where('user_id', $uId)->where('page', 'kode_dateformonth')->exists()) {

            $selectedFilters = UserCategory::all()->where('user_id', '=', $uId)->where('page', 'kode_dateformonth')->first();
            parse_str($selectedFilters->categories_list, $selectedFilters);


            //add default category
            $defaultColl = ['NAME_OBL', 'NAME_ST', 'IND_ST', 'YEAR_CH', 'MONTH_CH', 'T', 'OS'];
            if (isset($selectedFilters['collumns'])) {
                $collumns = $selectedFilters['collumns'];
            } else $collumns = [];

            $helper->addItemsinArr($defaultColl, $collumns);

            //Выборка категорий по выбраным пользователем колонкам
            foreach ($this->categories as $category) {
                foreach ($collumns as $collumn) {
                    if ($category['code_col_name'] == $collumn) {
                        $categories[] = $category;
                    }
                }
            }

            // Превращаю строку в массив ибо есть колонки с месяцем и годом в 1-м поле input
            $dateFrom = explode("-", $selectedFilters['dateFrom']);
            $dateTo = explode("-", $selectedFilters['dateTo']);

            /**
             * Data filtering
             */
            if (isset($data['regionName']) && empty($data['stationName']))
            {
                $dataForTable = DB::table('CAT_STATION')
                    ->join('CAT_OBL', 'CAT_STATION.OBL_ID', '=', 'CAT_OBL.OBL_ID')
                    ->join('TOS_MES', 'CAT_STATION.IND_ST', '=', 'TOS_MES.IND_ST')
                    ->select('CAT_OBL.NAME_OBL', 'CAT_STATION.NAME_ST', 'TOS_MES.IND_ST', 'TOS_MES.YEAR_CH', 'TOS_MES.MONTH_CH', 'TOS_MES.T', 'TOS_MES.OS')
                    ->orderBy('CAT_STATION.OBL_ID', 'asc')
                    ->orderBy('CAT_STATION.IND_ST')
                    ->whereIn('CAT_STATION.OBL_ID', $selectedFilters['regionName'])
                    ->whereBetween('YEAR_CH', [$dateFrom[0], $dateTo[0]])
                    ->whereBetween('MONTH_CH', [$dateFrom[1], $dateTo[1]])
                    ->get();
            }
            else if (isset($data['regionName']) && isset($data['stationName']))
            {
                $dataForTable = DB::table('CAT_STATION')
                    ->join('CAT_OBL', 'CAT_STATION.OBL_ID', '=', 'CAT_OBL.OBL_ID')
                    ->join('TOS_MES', 'CAT_STATION.IND_ST', '=', 'TOS_MES.IND_ST')
                    ->select('CAT_OBL.NAME_OBL', 'CAT_STATION.NAME_ST', 'TOS_MES.IND_ST', 'TOS_MES.YEAR_CH', 'TOS_MES.MONTH_CH', 'TOS_MES.T', 'TOS_MES.OS')
                    ->orderBy('CAT_STATION.OBL_ID', 'asc')
                    ->orderBy('CAT_STATION.IND_ST')
                    ->whereIn('CAT_STATION.OBL_ID', $selectedFilters['regionName'])
                    ->whereIn('CAT_STATION.IND_ST', $selectedFilters['stationName'])
                    ->whereBetween('YEAR_CH', [$dateFrom[0], $dateTo[0]])
                    ->whereBetween('MONTH_CH', [$dateFrom[1], $dateTo[1]])
                    ->get();
            }
            else if (empty($data['regionName']) && isset($data['stationName']))
            {
                $dataForTable = DB::table('CAT_STATION')
                    ->join('CAT_OBL', 'CAT_STATION.OBL_ID', '=', 'CAT_OBL.OBL_ID')
                    ->join('TOS_MES', 'CAT_STATION.IND_ST', '=', 'TOS_MES.IND_ST')
                    ->select('CAT_OBL.NAME_OBL', 'CAT_STATION.NAME_ST', 'TOS_MES.IND_ST', 'TOS_MES.YEAR_CH', 'TOS_MES.MONTH_CH', 'TOS_MES.T', 'TOS_MES.OS')
                    ->orderBy('CAT_STATION.OBL_ID', 'asc')
                    ->orderBy('CAT_STATION.IND_ST')
                    ->whereIn('CAT_STATION.IND_ST', $selectedFilters['stationName'])
                    ->whereBetween('YEAR_CH', [$dateFrom[0], $dateTo[0]])
                    ->whereBetween('MONTH_CH', [$dateFrom[1], $dateTo[1]])
                    ->get();
            }
            else if (empty($data['regionName']) && empty($data['stationName']))
            {
                $dataForTable = DB::table('CAT_STATION')
                    ->join('CAT_OBL', 'CAT_STATION.OBL_ID', '=', 'CAT_OBL.OBL_ID')
                    ->join('TOS_MES', 'CAT_STATION.IND_ST', '=', 'TOS_MES.IND_ST')
                    ->select('CAT_OBL.NAME_OBL', 'CAT_STATION.NAME_ST', 'TOS_MES.IND_ST', 'TOS_MES.YEAR_CH', 'TOS_MES.MONTH_CH', 'TOS_MES.T', 'TOS_MES.OS')
                    ->orderBy('CAT_STATION.OBL_ID', 'asc')
                    ->orderBy('CAT_STATION.IND_ST')
                    ->whereBetween('YEAR_CH', [$dateFrom[0], $dateTo[0]])
                    ->whereBetween('MONTH_CH', [$dateFrom[1], $dateTo[1]])
                    ->get();
            }

            /**
             * array with all data for view
             */

            $this->data = [
                'categories' => $this->categories,
                'dataForTable' => $dataForTable,
                'selectedCategories' => $categories,
            ];
        }

        if (count($dataForTable) >= 50000)
        {
            $this->data = [
                'errors' => 'Data limit is limited'
            ];
        }

        return view('site.dateformonth.KNDailyExport', $this->data);
    }
}
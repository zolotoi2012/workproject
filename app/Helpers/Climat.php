<?php

namespace App\Helpers;

use Illuminate\Support\Collection;

class Climat
{
    /**
     * @var array
     */
    private $date = [];

    /**
     * @var false|string
     */
   // private $dateBeforeFromDate;

    /**
     * @var Collection
     */
    private $allData;

    /**
     * @var array
     */
    private $allDataSort = [];

    /**
     * @var array
     */
    private $categories = [];

    /**
     * @var array
     */
    private $dataForTable = [];


    public function __construct(Collection $allData, array $date, array $categories)
    {
        $this->date = $date;
        // $this->dateBeforeFromDate = date('Y-n', (strtotime($date['dateFrom']) - (60 * 60 * 24)));
        $this->allData = $allData;
        array_splice($categories, 0, 4);
        $this->categories = $categories;
        $this->createDefArr();
    }

    private function createDefArr()
    {
        $dateFrom = strtotime($this->date['dateFrom']);
        $dateTo = strtotime($this->date['dateTo']);
        while ($dateFrom <= $dateTo) {
            foreach ($this->allData as $item) {
                if (strtotime($item->DATE_CH) == $dateFrom) {
                    $newData = [
                        'NAME_OBL' => $item->NAME_OBL,
                        'NAME_ST' => $item->NAME_ST,
                        'IND_ST' => $item->IND_ST,
                        'MONTH_CH' => $item->MONTH_CH,
                        'YEAR_CH' => $item->YEAR_CH
                    ];
                    in_array($newData, $this->dataForTable) ?: $this->dataForTable[] = $newData;
                }
            }
            $dateFrom = $dateFrom + 60 * 60 * 24;
        }
        return $this;
    }

    public function calculate()
    {
//        return $this->dataForTable;
        //В запитах розібратись з датою, зробити щоб виводило на всі вказані дти
        foreach ($this->dataForTable as &$forTable) {
            $date = $forTable['MONTH_CH'];
            $prevDate = date('Y-n', strtotime($forTable['DATE_CH']) - (60 * 60 * 24));

            $allDataSort = [];
            //Для @var $forTable беру колекцію відсортовану по індексу станції та даті
            $allDataSort[] = collect($this->allData)
                ->where('IND_ST', '=', $forTable['IND_ST'])
                ->where('DATE_CH', '=', $prevDate)
                ->first();

            $tmp = collect($this->allData)
                ->where('IND_ST', '=', $forTable['IND_ST'])
                ->where('DATE_CH', '=', $date)//$this->date['dateFrom']
                ->all();

            $this->allDataSort = array_merge($allDataSort, $tmp);

            //виклик функцій обрахунку за обраними користувачем параметрами
            foreach ($this->categories as $item) {
                try {
                    $newItems = call_user_func([__CLASS__, strtolower($item['code_col_name'])], $this->allDataSort);
                    $forTable = array_merge($forTable, $newItems);
                } catch (\Exception $e) {
                    var_dump($e->getMessage());
                }
            }
        }
        return $this->dataForTable;
    }

    public function ppp($dataForTableStr){
        $countItems = count($dataForTableStr);
        $sum = 0;
        foreach ($dataForTableStr as $item) {
            $item = (object)$item;
            if (property_exists($item, 'PPP')) {
                $sum += $item->PPP;
            }
        }
        $avg = round($sum / $countItems, 1);
        return [
            "PPP" => $avg
        ];
    }

    private function ttt($dataForTableStr)
    {
        $countItems = count($dataForTableStr);
        $sum = 0;
        foreach ($dataForTableStr as $item) {
            $item = (object)$item;
            if (property_exists($item, 'TTT')) {
                $sum += $item->TTT;
            }
        }
        $avg = round($sum / $countItems, 1);
        return [
            "TTT" => $avg
        ];
    }

    private function ststst($dataForTableStr)
    {
        $countItems = count($dataForTableStr);
        $sum = 0;
        foreach ($dataForTableStr as $item) {
            $item = (object)$item;
            if (property_exists($item, 'STSTST')) {
                $sum += $item->STSTST;
            }
        }
        $avg = round($sum / $countItems, 1);
        return [
            "STSTST" => $avg
        ];
    }

    private function txtxtx($dataForTableStr)
    {
        $countItems = count($dataForTableStr);
        $sum = 0;
        foreach ($dataForTableStr as $item) {
            $item = (object)$item;
            if (property_exists($item, 'TXTXTX')) {
                $sum += $item->TXTXTX;
            }
        }
        $avg = round($sum / $countItems, 1);
        return [
            "TXTXTX" => $avg
        ];
    }

    private function tntntn($dataForTableStr)
    {
        $countItems = count($dataForTableStr);
        $sum = 0;
        foreach ($dataForTableStr as $item) {
            $item = (object)$item;
            if (property_exists($item, 'TNTNTN')) {
                $sum += $item->TNTNTN;
            }
        }
        $avg = round($sum / $countItems, 1);
        return [
            "TNTNTN" => $avg
        ];
    }

    private function eee($dataForTableStr)
    {
        $countItems = count($dataForTableStr);
        $sum = 0;
        foreach ($dataForTableStr as $item) {
            $item = (object)$item;
            if (property_exists($item, 'EEE')) {
                $sum += $item->EEE;
            }
        }
        $avg = round($sum / $countItems, 1);
        return [
            "EEE" => $avg
        ];
    }

    private function r1r1r1r1($dataForTableStr)
    {
        $countItems = count($dataForTableStr);
        $sum = 0;
        foreach ($dataForTableStr as $item) {
            $item = (object)$item;
            if (property_exists($item, 'R1R1R1R1')) {
                $sum += $item->R1R1R1R1;
            }
        }
        $avg = round($sum / $countItems, 1);
        return [
            "R1R1R1R1" => $avg
        ];
    }

    private function rd($dataForTableStr)
    {
        $countItems = count($dataForTableStr);
        $sum = 0;
        foreach ($dataForTableStr as $item) {
            $item = (object)$item;
            if (property_exists($item, 'RD')) {
                $sum += $item->RD;
            }
        }
        $avg = round($sum / $countItems, 1);
        return [
            "RD" => $avg
        ];
    }

    private function nrnr($dataForTableStr)
    {
        $countItems = count($dataForTableStr);
        $sum = 0;
        foreach ($dataForTableStr as $item) {
            $item = (object)$item;
            if (property_exists($item, 'NRNR')) {
                $sum += $item->NRNR;
            }
        }
        $avg = round($sum / $countItems, 1);
        return [
            "NRNR" => $avg
        ];
    }

    private function s1s1s1($dataForTableStr)
    {
        $countItems = count($dataForTableStr);
        $sum = 0;
        foreach ($dataForTableStr as $item) {
            $item = (object)$item;
            if (property_exists($item, 'S1S1S1')) {
                $sum += $item->S1S1S1;
            }
        }
        $avg = round($sum / $countItems, 1);
        return [
            "S1S1S1" => $avg
        ];
    }

    private function pspsps($dataForTableStr)
    {
        $countItems = count($dataForTableStr);
        $sum = 0;
        foreach ($dataForTableStr as $item) {
            $item = (object)$item;
            if (property_exists($item, 'PSPSPS')) {
                $sum += $item->PSPSPS;
            }
        }
        $avg = round($sum / $countItems, 1);
        return [
            "PSPSPS" => $avg
        ];
    }

    private function mpmp($dataForTableStr)
    {
        $countItems = count($dataForTableStr);
        $sum = 0;
        foreach ($dataForTableStr as $item) {
            $item = (object)$item;
            if (property_exists($item, 'MPMP')) {
                $sum += $item->MPMP;
            }
        }
        $avg = round($sum / $countItems, 1);
        return [
            "MPMP" => $avg
        ];
    }

    private function mtmt($dataForTableStr)
    {
        $countItems = count($dataForTableStr);
        $sum = 0;
        foreach ($dataForTableStr as $item) {
            $item = (object)$item;
            if (property_exists($item, 'MTMT')) {
                $sum += $item->MTMT;
            }
        }
        $avg = round($sum / $countItems, 1);
        return [
            "MTMT" => $avg
        ];
    }

    private function mtx($dataForTableStr)
    {
        $countItems = count($dataForTableStr);
        $sum = 0;
        foreach ($dataForTableStr as $item) {
            $item = (object)$item;
            if (property_exists($item, 'MTX')) {
                $sum += $item->MTX;
            }
        }
        $avg = round($sum / $countItems, 1);
        return [
            "MTX" => $avg
        ];
    }

    private function mtn($dataForTableStr)
    {
        $countItems = count($dataForTableStr);
        $sum = 0;
        foreach ($dataForTableStr as $item) {
            $item = (object)$item;
            if (property_exists($item, 'MTN')) {
                $sum += $item->MTN;
            }
        }
        $avg = round($sum / $countItems, 1);
        return [
            "MTN" => $avg
        ];
    }

    private function meme($dataForTableStr)
    {
        $countItems = count($dataForTableStr);
        $sum = 0;
        foreach ($dataForTableStr as $item) {
            $item = (object)$item;
            if (property_exists($item, 'MEME')) {
                $sum += $item->MEME;
            }
        }
        $avg = round($sum / $countItems, 1);
        return [
            "MEME" => $avg
        ];
    }

    private function mrmr($dataForTableStr)
    {
        $countItems = count($dataForTableStr);
        $sum = 0;
        foreach ($dataForTableStr as $item) {
            $item = (object)$item;
            if (property_exists($item, 'MRMR')) {
                $sum += $item->MRMR;
            }
        }
        $avg = round($sum / $countItems, 1);
        return [
            "MRMR" => $avg
        ];
    }

    private function msms($dataForTableStr)
    {
        $countItems = count($dataForTableStr);
        $sum = 0;
        foreach ($dataForTableStr as $item) {
            $item = (object)$item;
            if (property_exists($item, 'MSMS')) {
                $sum += $item->MSMS;
            }
        }
        $avg = round($sum / $countItems, 1);
        return [
            "MSMS" => $avg
        ];
    }
}
<?php

namespace App\Helpers;

use Illuminate\Support\Collection;

/**
 * Class KNDaily
 * @package App\Helpers
 */
class DateMonth
{
    /**
     * @var array
     */
    private $date = [];

    /**
     * @var false|string
     */
    private $dateBeforeFromDate;

    /**
     * @var Collection
     */
    private $allData;

    /**
     * @var array
     */
    private $allDataSort = [];

    /**
     * @var array
     */
    private $categories = [];

    /**
     * @var array
     */
    private $dataForTable = [];

    /**
     * KNDaily constructor.
     * @param Collection $allData
     * @param array $date
     * @param array $categories
     */
    public function __construct(Collection $allData, array $date, array $categories)
    {
        $this->date = $date;
        $this->dateBeforeFromDate = date('m-Y', (strtotime($date['dateFrom']) - (60 * 60 * 24)));
        $this->allData = $allData;
        array_splice($categories, 0, 4);
        $this->categories = $categories;
        $this->createDefArr();
    }

    /**
     * @return $this
     */
    private function createDefArr()
    {
        $dateFrom = strtotime($this->date['dateFrom']);
        $dateTo = strtotime($this->date['dateTo']);
        while ($dateFrom <= $dateTo) {
            foreach ($this->allData as $item) {
                if (strtotime($item->DATE_CH) == $dateFrom) {
                    $newData =
                        [
                        'NAME_OBL' => $item->NAME_OBL,
                        'NAME_ST' => $item->NAME_ST,
                        'IND_ST' => $item->IND_ST,
                        'YEAR_CH' => $item->YEAR_CH,
                        'MONTH_CH' => $item->MONTH_CH,
                        ];
                    in_array($newData, $this->dataForTable) ?: $this->dataForTable[] = $newData;
                }
            }
            $dateFrom = $dateFrom + 60 * 60 * 24;
        }
        return $this;
    }

    /**
     * Call methods for calculating data
     */
    public function calculate()
    {
//        return $this->dataForTable;
        //В запитах розібратись з датою, зробити щоб виводило на всі вказані дти
        foreach ($this->dataForTable as &$forTable) {
            $date = $forTable['YEAR_CH'];
            $date1 = $forTable['MONTH_CH'];
            $prevDate = date('m-Y', strtotime($forTable['DATE_CH']) - (60 * 60 * 24));
                $prevDate = explode(".", $dateFrom);
                $monthFrom = array_shift($dateFrom);
                $yearFrom = array_pop($dateFrom);

            $allDataSort = [];
            //Для @var $forTable беру колекцію відсортовану по індексу станції та даті
            $allDataSort[] = collect($this->allData)
                ->where('IND_ST', '=', $forTable['IND_ST'])
                /*->where('SROK_CH', '=', 21)*/
                ->where('MONTH_CH', '=', $prevDate)
                ->where('YEAR_CH', '=', $prevDate)
                ->first();
            $tmp = collect($this->allData)
                ->where('IND_ST', '=', $forTable['IND_ST'])
               /* ->whereIn('SROK_CH', [0, 3, 6, 9, 12, 15, 18])*/
                ->where('YEAR_CH', '=', $date)//$this->date['dateFrom']
                ->where('MONTH_CH', '=', $date1)
                ->all();

            $this->allDataSort = array_merge($allDataSort, $tmp);

            //виклик функцій обрахунку за обраними користувачем параметрами
            foreach ($this->categories as $item) {
                try {
                    $newItems = call_user_func([__CLASS__, strtolower($item['code_col_name'])], $this->allDataSort);
                    $forTable = array_merge($forTable, $newItems);
                } catch (\Exception $e) {
                    var_dump($e->getMessage());
                }
            }
        }
        return $this->dataForTable;
    }

    /**
     * @param $srokDataForTableStr
     * @return array
     *
     * Середня температура
     */
    private function ttt($srokDataForTableStr)
    {
        $countItems = count($srokDataForTableStr);
        $sum = 0;
        foreach ($srokDataForTableStr as $item) {
            $item = (object)$item;
            if (property_exists($item, 'T')) {
                $sum += $item->TTT;
            }
        }
        $avg = round($sum / $countItems, 1);
        return [
            "TTT" => $avg
        ];
    }

    /**
     * @param $srokDataForTableStr
     * @return array
     *
     * Сумарна кількість опадів
     */
    /*private function sumop($srokDataForTableStr)
    {
        $SUM_OP = null;
        foreach ($srokDataForTableStr as $item) {
            $item = (object)$item;
            if (property_exists($item, 'SROK_CH') && property_exists($item, 'SUM_OP')) {
                if ($item->SROK_CH == 6 && $item->SUMOP != null || $item->SROK_CH == 18 && $item->SUM_OP != null) {
                    $SUM_OP += $item->SUM_OP;
                }
            }
        }
        return [
            "SUM_OP" => $SUM_OP
        ];
    }*/

    // добова сума опадів (сума)

    private function rrr1($srokDataForTableStr)
    {
        $rrr1 = null;
        foreach ($srokDataForTableStr as $item) {
            $item = (object)$item;
            if (property_exists($item, 'MONTH_CH') && property_exists($item, 'RRR1')) {
                if ($item->SROK_CH == 6 && $item->RRR1 != null || $item->SROK_CH == 18 && $item->RRR1 != null) {
                    $rrr1 += $item->RRR1;
                }
            }
        }
        return [
            "RRR1" => $rrr1
        ];
    }
}
<?php

namespace App\Http\Controllers;

use App\Helpers\{
    Helper, Climat
};
use App\{
    Region, Station, UserCategory
};

use DB;
use Auth;

class ClimatAVGController extends Controller
{
    private $categories = [
        [
            'col_name' => 'Назва області',
            'short_col_name' => 'Назва області',
            'code_col_name' => 'NAME_OBL'
        ],
        [
            'col_name' => 'Назва станції',
            'short_col_name' => 'Назва станції',
            'code_col_name' => 'NAME_ST'
        ],
        [
            'col_name' => 'Індекс',
            'short_col_name' => 'Індекс',
            'code_col_name' => 'IND_ST'
        ],
        [
            'col_name' => 'Рік',
            'short_col_name' => 'Рік',
            'code_col_name' => 'YEAR_CH'
        ],
        [
            'col_name' => 'Місяць',
            'short_col_name' => 'Місяць',
            'code_col_name' => 'MONTH_CH'
        ],
        [
            'col_name' => 'Р, гПа(моря)',
            'short_col_name' => 'Р, гПа(моря)',
            'code_col_name' => 'PPP'
        ],
        [
            'col_name' => 'T',
            'short_col_name' => 'T',
            'code_col_name' => 'TTT'
        ],
        [
            'col_name' => 'dT сер.кв.відх',
            'short_col_name' => 'dT сер.кв.відх',
            'code_col_name' => 'STSTST'
        ],
        [
            'col_name' => 'Tmax',
            'short_col_name' => 'Tmax',
            'code_col_name' => 'TXTXTX'
        ],
        [
            'col_name' => 'Tmin',
            'short_col_name' => 'Tmin',
            'code_col_name' => 'TNTNTN'
        ],
        [
            'col_name' => 'E, гПа(парц.тиск)',
            'short_col_name' => 'E, гПа(парц.тиск)',
            'code_col_name' => 'EEE'
        ],
        [
            'col_name' => 'Сума опадів',
            'short_col_name' => 'Сума опадів',
            'code_col_name' => 'R1R1R1R1'
        ],
        [
            'col_name' => 'Повтор R',
            'short_col_name' => 'Повтор R',
            'code_col_name' => 'RD'
        ],
        [
            'col_name' => 'Повтор R',
            'short_col_name' => 'Повтор R',
            'code_col_name' => 'RD'
        ],
        [
            'col_name' => 'R > 1мм(діб)',
            'short_col_name' => 'R > 1мм(діб)',
            'code_col_name' => 'NRNR'
        ],
        [
            'col_name' => 'Сон сяйва, год',
            'short_col_name' => 'Сон сяйва, год',
            'code_col_name' => 'S1S1S1'
        ],
        [
            'col_name' => '% сон сяйва (до норми)',
            'short_col_name' => '% сон сяйва (до норм)',
            'code_col_name' => 'PSPSPS'
        ],
        [
            'col_name' => 'Відс. дані P,діб',
            'short_col_name' => 'Відс. дані Р, діб',
            'code_col_name' => 'MPMP'
        ],
        [
            'col_name' => 'Відс. дані T,діб',
            'short_col_name' => 'Відс. дані T,діб',
            'code_col_name' => 'MTMT'
        ],
        [
            'col_name' => 'Відс. дані Tmax,діб',
            'short_col_name' => 'Відс. дані Tmax,діб',
            'code_col_name' => 'MTX'
        ],
        [
            'col_name' => 'Відс. дані Tmin,діб',
            'short_col_name' => 'Відс. дані Tmin,діб',
            'code_col_name' => 'MTN'
        ],
        [
            'col_name' => 'Відс. дані E,діб',
            'short_col_name' => 'Відс. дані E,діб',
            'code_col_name' => 'MEME'
        ],
        [
            'col_name' => 'Відс. дані R,діб',
            'short_col_name' => 'Відс. дані R,діб',
            'code_col_name' => 'MRMR'
        ],
        [
            'col_name' => 'Відс. дані S,діб',
            'short_col_name' => 'Відс. дані S,діб',
            'code_col_name' => 'MSMS'
        ],
    ];

    private $collumns = ['NAME_OBL', 'NAME_ST', 'IND_ST', 'YEAR_CH', 'MONTH_CH', 'PPP', 'TTT', 'STSTST', 'TXTXTX', 'TNTNTN', 'EEE', 'R1R1R1R1', 'RD', 'NRNR', 'S1S1S1', 'PSPSPS', 'MPMP', 'MTMT', 'MTX', 'MTN', 'MEME', 'MRMR', 'MSMS' ];

    /**
     * Kode_knController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
        define("PER_PAGE", 18);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show()
    {

        $helper = new Helper();
        $regions = new Region();
        $stations = new Station();

        $uId = Auth::getUser()->getAuthIdentifier();

        if (DB::table('user_categories')->where('user_id', $uId)->where('page', 'kode_climat')->exists()) {
            $currentPage = isset($_GET['page']) ? $_GET['page'] : 1;

            $selectedFilters = UserCategory::all()->where('user_id', '=', $uId)->where('page', 'kode_climat')->first();
            parse_str($selectedFilters->categories_list, $selectedFilters);

            $selectedRegions = isset($selectedFilters['regionName']) ? $selectedFilters['regionName'] : null;
            $selectedStations = isset($selectedFilters['stationName']) ? $selectedFilters['stationName'] : null;

            //add default category
            $defaultColl = ['NAME_OBL', 'NAME_ST', 'IND_ST'];
            if (isset($selectedFilters['collumns'])) {
                $collumns = $selectedFilters['collumns'];
            } else $collumns = [];

            $helper->addItemsinArr($defaultColl, $collumns);

            //Выборка категорий по выбраным пользователем колонкам
            foreach ($this->categories as $category) {
                foreach ($collumns as $collumn) {
                    if ($category['code_col_name'] == $collumn) {
                        $categories[] = $category;
                    }
                }
            }

            $currentDate = date('Y-n');
            $dateFrom = date('Y-n', (strtotime($currentDate) - (60 * 60 * 24) * 2));
            $dateTo = date('Y-n', (strtotime($currentDate) - (60 * 60 * 24)));
            $selectedCategories = $this->categories;

            /**
             * Data filtering
             */
            if (isset($selectedFilters['regionName']) && empty($selectedFilters['stationName'])) {
                $dataForTable = DB::table('CAT_STATION')
                    ->join('CAT_OBL', 'CAT_STATION.OBL_ID', '=', 'CAT_OBL.OBL_ID')
                    ->join('CLIMAT_MONTH', 'CAT_STATION.IND_ST', '=', 'CLIMAT_MONTH.IND_ST')
                    ->whereIn('CAT_STATION.OBL_ID', $selectedFilters['regionName'])
                    ->orderBy('CAT_STATION.OBL_ID', 'asc')
                    ->orderBy('CAT_STATION.IND_ST')
                    ->get();

            } else if (isset($selectedFilters['regionName']) && isset($selectedFilters['stationName'])) {
                $dataForTable = DB::table('CAT_STATION')
                    ->join('CAT_OBL', 'CAT_STATION.OBL_ID', '=', 'CAT_OBL.OBL_ID')
                    ->join('CLIMAT_MONTH', 'CAT_STATION.IND_ST', '=', 'CLIMAT_MONTH.IND_ST')
                    ->whereIn('CAT_STATION.OBL_ID', $selectedFilters['regionName'])
                    ->whereIn('CAT_STATION.IND_ST', $selectedFilters['stationName'])
                    ->orderBy('CAT_STATION.OBL_ID', 'asc')
                    ->orderBy('CAT_STATION.IND_ST')
                    ->get();

            } else if (empty($selectedFilters['regionName']) && isset($selectedFilters['stationName'])) {
                $dataForTable = DB::table('CAT_STATION')
                    ->join('CAT_OBL', 'CAT_STATION.OBL_ID', '=', 'CAT_OBL.OBL_ID')
                    ->join('CLIMAT_MONTH', 'CAT_STATION.IND_ST', '=', 'CLIMAT_MONTH.IND_ST')
                    ->whereIn('CAT_STATION.IND_ST', $selectedFilters['stationName'])
                    ->orderBy('CAT_STATION.OBL_ID', 'asc')
                    ->orderBy('CAT_STATION.IND_ST')
                    ->get();

            } else if (empty($selectedFilters['regionName']) && empty($selectedFilters['stationName'])) {
                $dataForTable = DB::table('CAT_STATION')
                    ->join('CAT_OBL', 'CAT_STATION.OBL_ID', '=', 'CAT_OBL.OBL_ID')
                    ->join('CLIMAT_MONTH', 'CAT_STATION.IND_ST', '=', 'CLIMAT_MONTH.IND_ST')
                    ->orderBy('CAT_STATION.OBL_ID', 'asc')
                    ->orderBy('CAT_STATION.IND_ST')
                    ->get();
            }


            $countStr = count($dataForTable);
            $countPages = ceil($countStr / PER_PAGE);

            $paginationLinks = $countPages > 1 ? $helper->generateLinksForPagination(url('/climat_avg'), $countPages, $currentPage, true) : "";

            /**
             * array with all data for view
             */
            $data = [
                'regions' => $regions->getAllRegions(),
                'stations' => $stations->getAllStation(),
                'categories' => $this->categories,
                'selectedCategories' => $selectedCategories,
                'dataForTable' => $dataForTable->forPage($currentPage, PER_PAGE),
                'selectedRegions' => $selectedRegions,
                'selectedStations' => $selectedStations,
                'paginationLinks' => $paginationLinks,
                'dateFrom' => $dateFrom,
                'dateTo' => $dateTo
            ];

        } else {
            //if first auth
            $currentDate = date('Y-n');
            $dateFrom = date('Y-n', (strtotime($currentDate) - (60 * 60 * 24) * 2));
            $dateTo = date('Y-n', (strtotime($currentDate) - (60 * 60 * 24)));

            $selectedCategories = $this->categories;
            $currentPage = isset($_GET['page']) ? $_GET['page'] : 1;

            /**
             * Get data
             */
            $dataForTable = DB::table('CAT_STATION')
                ->select($this->collumns)
                ->join('CAT_OBL', 'CAT_STATION.OBL_ID', '=', 'CAT_OBL.OBL_ID')
                ->join('CLIMAT_MONTH', 'CAT_STATION.IND_ST', '=', 'CLIMAT_MONTH.IND_ST')
                ->orderBy('CAT_STATION.OBL_ID', 'asc')
                ->orderBy('CAT_STATION.IND_ST')
                ->get();

            /**
             * Create pagination links
             */
            $countPages = ceil(count($dataForTable) / PER_PAGE);
            $paginationLinks = $helper->generateLinksForPagination(url('/climat_avg'), $countPages, $currentPage, true);

            /**
             * array with all data for view
             */
            $data = [
                'regions' => $regions->getAllRegions(),
                'stations' => $stations->getAllStation(),
                'categories' => $this->categories,
                'dataForTable' => $dataForTable->forPage($currentPage, PER_PAGE),
                'selectedCategories' => $selectedCategories,
                'paginationLinks' => $paginationLinks,
                'dateTo' => $dateTo,
                'dateFrom' => $dateFrom
            ];
        }

        return view('/site.climat_avg.kode_climat', $data);
    }
        public function getData()
        {
        $helper = new Helper();
        $stations = new Station();

        parse_str($_POST['data'], $data);

        $ajaxIdentification = $data['requestName'];

        switch ($ajaxIdentification) {
            case "selectStation":
                {
                    if (empty($data['regionName'])) {
                        $stations = $stations->getAllStation();

                    } else {
                        $stations->regionName = $data['regionName'];
                        $stations = $stations->filterStation();
                    }

                    $data = [
                        'station' => $stations
                    ];

                    $response_data = json_decode($data, true);
                    return response($response_data, 200);
                    break;
                }

            case "selectInfoForTable":
                {
                    $currentPage = isset($data['page']) ? $data['page'] : 1;

                    //add default category
                    $defaultColl = ['NAME_OBL', 'NAME_ST', 'IND_ST', 'YEAR_CH', 'MONTH_CH'];
                    if (isset($data['collumns'])) {
                        $collumns = $data['collumns'];
                    } else $collumns = [];

                    $helper->addItemsinArr($defaultColl, $collumns);

                    //Выборка категорий по выбраным пользователем колонкам
                    foreach ($this->categories as $category) {
                        foreach ($collumns as $collumn) {
                            if ($category['code_col_name'] == $collumn) {
                                $categories[] = $category;
                            }
                        }
                    }

                    $uId = Auth::getUser()->getAuthIdentifier();
                    if (DB::table('user_categories')->where('user_id', $uId)->where('page', 'kode_climat')->exists())
                    {
                        DB::table('user_categories')->where('user_id', $uId)->where('page', 'kode_climat')->update(
                            ['categories_list' => $_POST['data']]
                        );
                    }
                    else
                    {
                        DB::table('user_categories')->where('user_id', $uId)->insert(
                            ['user_id' => $uId, 'page' => 'kode_climat', 'categories_list' => $_POST['data']]
                        );
                    }


                    /*
                    Дата из инпутов приходит в формате Y-m, с ведущим нулём,
                    а данные в таблице в формате int и без ведущего нуля
                    Из-за чего приходиться удалять 0 из получаемых данных от input
                     */

                    $dateFrom = $data['dateFrom'];
                    $dateTo = $data['dateTo'];

                    $dateFrom = explode('-', $dateFrom);
                    $dateTo = explode('-', $dateTo);

                    // занесение 5-го елемента строки input в переменную
                    /*$zero = substr($dateFrom, 5, 1);
                    $ziro = substr($dateTo, 5, 1);

                    // проверка: является ли 5-й елемент 0 (2018-01 \ 2018-11)
                    if($zero === '0' ) {
                        $dateFrom = substr_replace($dateFrom, '', 5, 1);
                    }

                    if($ziro === '0') {
                        $dateTo = substr_replace($dateTo, '', 5, 1);
                    }*/

                    $zeroFrom = substr($dateFrom[1], 0, 1);
                    $zeroTo = substr($dateTo[1], 0, 1);

                    // проверка: является ли 5-й елемент 0 (2018-01 \ 2018-11)
                    if($zeroFrom === '0' ) {
                        $dateFrom[1] = substr_replace($dateFrom[1], '', 0, 1);
                    }

                    if($zeroTo === '0') {
                        $dateTo[1] = substr_replace($dateTo[1], '', 0, 1);
                    }


                    /**
                     * Data filtering
                     */

                    if (isset($data['regionName']) && empty($data['stationName']))
                    {
                        $dataForTable = DB::table('CAT_STATION')
                            ->join('CAT_OBL', 'CAT_STATION.OBL_ID', '=', 'CAT_OBL.OBL_ID')
                            ->join('CLIMAT_MONTH', 'CAT_STATION.IND_ST', '=', 'CLIMAT_MONTH.IND_ST')
                            /*->where(DB::raw('DATE_FORMAT("%Y-%c", CONCAT(YEAR_CH, "-", MONTH_CH))'), "<=", DB::raw('DATE_FORMAT("%Y-%c", "$dateFrom")'))
                                ->where(DB::raw('DATE_FORMAT("%Y-%c", CONCAT(YEAR_CH, "-", MONTH_CH))'), ">=", DB::raw('DATE_FORMAT("%Y-%c", "$dateTo")'))
                                ->whereRaw(' DATE_FORMAT(CONCAT(YEAR_CH, "-", MONTH_CH), "%Y-%c") BETWEEN DATE_FORMAT("$dateFrom", "%Y-%c") AND DATE_FORMAT("$dateTo", "%Y-%c") ')
                                ->whereRaw(' DATE_FORMAT(CONCAT(YEAR_CH, "-", MONTH_CH), "%Y-%c") >= DATE_FORMAT('.$dateFrom.', "%Y-%c")
                        AND DATE_FORMAT(CONCAT(YEAR_CH, "-", MONTH_CH), "%Y-%c") <= DATE_FORMAT('.$dateTo.', "%Y-%c") ')*/
                            /* ->whereBetween(DB::raw('CONCAT(YEAR_CH, "-", MONTH_CH)' ), [$dateFrom, $dateTo])*/
                            ->whereBetween('YEAR_CH', [$dateFrom[0], $dateTo[0]])
                            ->whereBetween('MONTH_CH', [$dateFrom[1], $dateTo[1]])
                            /*[DB::raw('DATE_FORMAT('.$dateFrom.', "%Y-%c")'), DB::raw('DATE_FORMAT('.$dateTo.', "%Y-%c")')]*/
                            ->whereIn('CAT_STATION.OBL_ID', $data['regionName'])
                            ->orderBy('CAT_STATION.OBL_ID', 'asc')
                            ->orderBy('CAT_STATION.IND_ST')
                            /*->where(DB::raw('CONCAT(YEAR_CH, "-", MONTH_CH)'), "<=", $dateTo)
                                ->whereRaw('CONCAT(YEAR_CH, "-", MONTH_CH) BETWEEN '.$dateFrom.' AND '.$dateTo.' ')*/
                            ->get();
                    }

                    else if (isset($data['regionName']) && isset($data['stationName'])) {
                        $dataForTable = DB::table('CAT_STATION')
                            ->join('CAT_OBL', 'CAT_STATION.OBL_ID', '=', 'CAT_OBL.OBL_ID')
                            ->join('CLIMAT_MONTH', 'CAT_STATION.IND_ST', '=', 'CLIMAT_MONTH.IND_ST')
                            ->whereBetween('YEAR_CH', [$dateFrom[0], $dateTo[0]])
                            ->whereBetween('MONTH_CH', [$dateFrom[1], $dateTo[1]])
                            ->whereIn('CAT_STATION.OBL_ID', $data['regionName'])
                            ->whereIn('CAT_STATION.IND_ST', $data['stationName'])
                            ->orderBy('CAT_STATION.OBL_ID', 'asc')
                            ->orderBy('CAT_STATION.IND_ST')
                            /* ->whereRaw(' DATE_FORMAT( \'%Y-%m\'), $dateFrom ) <= DATE_FORMAT( (\'%Y-%m\'), CONCAT(YEAR_CH, '-', MONTH_CH))
                               AND DATE_FORMAT(\'%Y-%m\'), $dateTo) >= DATE_FORMAT(CONCAT(YEAR_CH, '-', MONTH_CH)) ')
                            ->whereRaw(' DATE_FORMAT(("%Y-%c"), \'$dateFrom\') <= DATE_FORMAT("%Y-%c", CONCAT(YEAR_CH, "-", MONTH_CH))
                                             AND DATE_FORMAT(("%Y-%c"), \'$dateTo\') >= DATE_FORMAT("%Y-%c", CONCAT(YEAR_CH, "-", MONTH_CH))')
                            ->whereBetween(DB::raw('DATE_FORMAT("%Y-%c", CONCAT(YEAR_CH, "-", MONTH_CH)'), [$dateFrom, $dateTo])
                            ->whereDate(DB::raw(' DATE_FORMAT("%Y-%c", CONCAT(YEAR_CH, "-", MONTH_CH))'), $dateFrom)
                            ->whereDate(DB::raw(' DATE_FORMAT("%Y-%c", CONCAT(YEAR_CH, "-", MONTH_CH))'), $dateTo)*/
                            ->get();
                    }
                    /*
                     * $dateFrom = date("Y-n");
                     * $dateTo = date("Y-n");
                     * WHERE CONCAT(YEAR_CH, '-', MONTH_CH) as dateFrom
                     * DATE_FORMAT(dateFrom, "%Y-%m") <= $dateFrom AND CONCAT(YEAR_CH, '-', 'MONTH_CH') as dateTo DATE_FORMAT(dateTo, "%Y-%m") >= $dateTo
                     *
                     *
                     *
                     * SELECT * FROM CLIMAT_MONTH
                     * INNER JOIN CAT_STATION on CAT_STATION.IND_ST = CLIMAT_MONTH.IND_ST
                     * INNER JOIN CAT_STATION.OBL_ID = CAT_OBL.OBL_ID
                     * WHERE YEAR_CH BETWEEN $dateFrom, $dateTo AND MONTH_CH BETWEEN $dateFrom AND $dateTo
                     *
                     *
                     *
                     *
                     * */


                    else if (empty($data['regionName']) && isset($data['stationName'])) {
                        $dataForTable = DB::table('CAT_STATION')
                            ->join('CAT_OBL', 'CAT_STATION.OBL_ID', '=', 'CAT_OBL.OBL_ID')
                            ->join('CLIMAT_MONTH', 'CAT_STATION.IND_ST', '=', 'CLIMAT_MONTH.IND_ST')
                            ->whereIn('CAT_STATION.IND_ST', $data['stationName'])
                            ->whereBetween('YEAR_CH', [$dateFrom[0], $dateTo[0]])
                            ->whereBetween('MONTH_CH', [$dateFrom[1], $dateTo[1]])
                            ->orderBy('CAT_STATION.OBL_ID', 'asc')
                            ->orderBy('CAT_STATION.IND_ST')
                            ->get();
                    }

                    else if (empty($data['regionName']) && empty($data['stationName'])) {
                        $dataForTable = DB::table('CAT_STATION')
                            ->join('CAT_OBL', 'CAT_STATION.OBL_ID', '=', 'CAT_OBL.OBL_ID')
                            ->join('CLIMAT_MONTH', 'CAT_STATION.IND_ST', '=', 'CLIMAT_MONTH.IND_ST')
                            ->whereBetween('YEAR_CH', [$dateFrom[0], $dateTo[0]])
                            ->whereBetween('MONTH_CH', [$dateFrom[1], $dateTo[1]])
                            ->orderBy('CAT_STATION.OBL_ID', 'asc')
                            ->orderBy('CAT_STATION.IND_ST')
                            ->get();
                    }

                   /* $climat = new Climat($dataForTable, ['dateFrom' => $dateFrom, 'dateTo' => $dateTo], $categories);
                    $tmp = $climat->calculate();

                    $dataForTable = collect($tmp); */

                    $countStr = count($dataForTable);
                    $countPages = ceil($countStr / PER_PAGE);

                    $paginationLinks = $countPages > 1 ? $helper->generateLinksForPagination(url('/climat_avg'), $countPages, $currentPage, true) : "";

                    $dataOut =
                        [
                            'categories' => $this->categories,
                            'selectedCategories' => $categories,
                            'dataForTable' => $dataForTable->forPage($currentPage, PER_PAGE),
                            'countPages' => $countPages,
                            'currentPage' => $currentPage,
                            'paginationLinks' => $paginationLinks,
                        ];

                    return view('site.climat_avg.table', $dataOut);
                    break;
                }
        }
    }
}


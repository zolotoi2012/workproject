<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\{
    Region, Station, UserCategory
};

use App\Helpers\{
    Helper // DateMonth
};

use DB;
use Auth;



class DateForMonth extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        define("PER_PAGE", 18);
    }

    private $categories = [
        [
            'col_name' => 'Назва області',
            'short_col_name' => 'Назва області',
            'code_col_name' => 'NAME_OBL'
        ],
        [
            'col_name' => 'Назва станції',
            'short_col_name' => 'Назва станції',
            'code_col_name' => 'NAME_ST'
        ],
        [
            'col_name' => 'Індекс',
            'short_col_name' => 'Індекс',
            'code_col_name' => 'IND_ST'
        ],
        [
            'col_name' => 'Місяць',
            'short_col_name' => 'Місяць',
            'code_col_name' => 'MONTH_CH'
        ],
        [
            'col_name' => 'Рік',
            'short_col_name' => 'Рік',
            'code_col_name' => 'YEAR_CH'
        ],
        [
            'col_name' => 'Температура повітря, середня',
            'short_col_name' => 'T сер, &deg;С',
            'code_col_name' => 'T'
        ],
        [
            'col_name' => 'Сума опадів',
            'short_col_name' => 'Сума опадів',
            'code_col_name' => 'OS'
        ],
    ];

    private $collumns = ['NAME_OBL', 'NAME_ST', 'TOS_MES.IND_ST', 'MONTH_CH', 'YEAR_CH', 'T', 'OS'];

    public function show()
    {
        $helper = new Helper();
        $regions = new Region();
        $stations = new Station();

        $uId = Auth::getUser()->getAuthIdentifier();

        if (DB::table('user_categories')->where('user_id', $uId)->where('page', 'dateformonth')->exists()) {
           // compact $date[] -> ???
            $currentDate = date('Y-m'); // Y-m-d ???
            $currentPage = isset($_GET['page']) ? $_GET['page'] : 1;

            $selectedFilters = UserCategory::all()->where('user_id', '=', $uId)->where('page', 'dateformonth')->first();
            parse_str($selectedFilters->categories_list, $selectedFilters);

            $selectedRegions = isset($selectedFilters['regionName']) ? $selectedFilters['regionName'] : null;
            $selectesStations = isset($selectedFilters['stationName']) ? $selectedFilters['stationName'] : null;

            //add default category
            $defaultColl = ['NAME_OBL', 'NAME_ST', 'IND_ST'];
            if (isset($selectedFilters['collumns'])) {
                $collumns = $selectedFilters['collumns'];
            } else $collumns = [];

            $helper->addItemsinArr($defaultColl, $collumns);

            //Выборка категорий по выбраным пользователем колонкам
            foreach ($this->categories as $category) {
                foreach ($collumns as $collumn) {
                    if ($category['code_col_name'] == $collumn) {
                        $categories[] = $category;
                    }
                }
            }

           /* $dateFrom = compact("dateFromMonth", "dateFromYear");
            $dateTo = compact("dateToMonth", "dateToYear");*/

           // превращаю получаемую строку с input для разделения в массив и занесением в переменные

            $dateFrom = date('m-Y', (strtotime($selectedFilters['dateFrom']) - (60 * 60 * 24)));

           /* Разбиваю строку в массив дабы поместить в переменные значения месяца и года*/

           $dateFrom = explode(".", $dateFrom);
               $monthFrom = array_shift($dateFrom);
               $yearFrom = array_pop($dateFrom);
           $dateTo = $selectedFilters['dateTo'];
            if ($selectedFilters['dateTo'] == $currentDate) {
                if ($selectedFilters['dateFrom'] == $selectedFilters['dateTo']) {
                    $dateFrom = date('m-Y', (strtotime($selectedFilters['dateFrom']) - ((60 * 60 * 24) * 2)));
                    $dateFrom = explode(".", $dateFrom);
                    $monthFrom = array_shift($dateFrom);
                    $yearFrom = array_pop($dateFrom);
                }
                $dateTo = date('m-Y', (strtotime($selectedFilters['dateTo']) - (60 * 60 * 24)));
                    $dateTo = explode(".", $dateTo);
                    $monthTo = array_shift($dateTo);
                    $yearTo = array_pop($dateTo);
            }


            /**
             * Data filtering
             */
            if (isset($selectedFilters['regionName']) && empty($selectedFilters['stationName'])) {
                $dataForTable = DB::table('CAT_STATION')
                    ->join('CAT_OBL', 'CAT_STATION.OBL_ID', '=', 'CAT_OBL.OBL_ID')
                    ->join('TOS_MES', 'CAT_STATION.IND_ST', '=', 'TOS_MES.IND_ST')
                    ->orderBy('CAT_STATION.OBL_ID', 'asc')
                    ->orderBy('CAT_STATION.IND_ST')
                    ->whereIn('CAT_STATION.OBL_ID', $selectedFilters['regionName'])
                    ->whereBetween('YEAR_CH', [$yearFrom, $yearTo])
                    ->whereBetween('MONTH_CH', [$monthFrom, $monthTo])
                    ->get();


            } else if (isset($selectedFilters['regionName']) && isset($selectedFilters['stationName'])) {
                $dataForTable = DB::table('CAT_STATION')
                    ->join('CAT_OBL', 'CAT_STATION.OBL_ID', '=', 'CAT_OBL.OBL_ID')
                    ->join('TOS_MES', 'CAT_STATION.IND_ST', '=', 'TOS_MES.IND_ST')
                    ->orderBy('CAT_STATION.OBL_ID', 'asc')
                    ->orderBy('CAT_STATION.IND_ST')
                    ->whereIn('CAT_STATION.OBL_ID', $selectedFilters['regionName'])
                    ->whereIn('CAT_STATION.IND_ST', $selectedFilters['stationName'])
                   /* ->whereBetween('DATE_CH', [$dateFrom, $dateTo])*/
                   ->whereBetween('YEAR_CH', [$yearFrom, $yearTo])
                    ->whereBetween('MONTH_CH', [$monthFrom, $monthTo])
                    ->get();

            } else if (empty($selectedFilters['regionName']) && isset($selectedFilters['stationName'])) {
                $dataForTable = DB::table('CAT_STATION')
                    ->join('CAT_OBL', 'CAT_STATION.OBL_ID', '=', 'CAT_OBL.OBL_ID')
                    ->join('TOS_MES', 'CAT_STATION.IND_ST', '=', 'TOS_MES.IND_ST')
                    ->orderBy('CAT_STATION.OBL_ID', 'asc')
                    ->orderBy('CAT_STATION.IND_ST')
                    ->whereIn('CAT_STATION.IND_ST', $selectedFilters['stationName'])
                    ->whereBetween('YEAR_CH', [$yearFrom, $yearTo])
                    ->whereBetween('MONTH_CH', [$monthFrom, $monthTo])
                    ->get();

            } else if (empty($selectedFilters['regionName']) && empty($selectedFilters['stationName'])) {
                $dataForTable = DB::table('CAT_STATION')
                    ->join('CAT_OBL', 'CAT_STATION.OBL_ID', '=', 'CAT_OBL.OBL_ID')
                    ->join('TOS_MES', 'CAT_STATION.IND_ST', '=', 'TOS_MES.IND_ST')
                    ->orderBy('CAT_STATION.OBL_ID', 'asc')
                    ->orderBy('CAT_STATION.IND_ST')
                    ->whereBetween('YEAR_CH', [$yearFrom, $yearTo])
                    ->whereBetween('MONTH_CH', [$monthFrom, $monthTo])
                    ->get();
            }

            if ($selectedFilters['dateFrom'] == $currentDate) {
                $dateFrom = date('m-Y', (strtotime($selectedFilters['dateFrom']) - (60 * 60 * 24)));
            } else {
                $dateFrom = $selectedFilters['dateFrom'];
            }
           /* $kndaily = new DateMonth($dataForTable, ['dateFrom' => $dateFrom, 'dateTo' => $dateTo], $categories);
            $tmp = $kndaily->calculate();*/

            //$dataForTable = collect($tmp);

           /* $countStr = count($dataForTable);
            $countPages = ceil($countStr / PER_PAGE);

            $paginationLinks = $countPages > 1 ? $helper->generateLinksForPagination(url('/dateformonth'), $countPages, $currentPage, true) : "";*/

            /**
             * array with all data for view
             */
            // превращаю строки обратно в массив для хелпера DateMonth
            /*$dateTo = implode(',', $monthTo, $yearTo);
            $dateFrom = implode(',', $monthFrom, $yearFrom);*/
            $selfTo = array_merge($monthTo,$yearTo);
            $dateTo = implode(',', $selfTo);
            $selfFrom = array_merge($monthFrom, $yearFrom);
            $dateFrom = implode(',', $selfFrom);

            $data = [
                'regions' => $regions->getAllRegions(),
                'stations' => $stations->getAllStation(),
                'categories' => $this->categories,
                'selectedCategories' => $categories,
                //'dataForTable' => $dataForTable->forPage($currentPage, PER_PAGE),
                'selectedRegions' => $selectedRegions,
                'selectedStations' => $selectesStations,
             //   'paginationLinks' => $paginationLinks,
                'dateTo' => $dateTo,
                'dateFrom' => $dateFrom
            ];

        } else {
            //if first auth
            $currentDate = date('m-Y');
            $dateFrom = date('m-Y', (strtotime($currentDate) - (60 * 60 * 24) * 2));
                $dateFrom = explode(".", $dateFrom);
                $monthFrom = array_shift($dateFrom);
                $yearFrom = array_pop($dateFrom);
            $dateTo = date('m-Y', (strtotime($currentDate) - (60 * 60 * 24)));
                $dateTo = explode(".", $dateTo);
                $monthTo = array_shift($dateTo);
                $yearTo = array_pop($dateTo);

            $selectedCategories = $this->categories;
            $currentPage = isset($_GET['page']) ? $_GET['page'] : 1;

            /**
             * Get data
             */
            /*$dataForTable = DB::table('CAT_STATION')
                 // CAT_STATION$
                // 'NAME_OBL', 'NAME_ST', 'TOS_MES.IND_ST'
                    ->select($this->collumns)
                    ->simplePaginate(15)
                    ->join('CAT_OBL', 'CAT_OBL.OBL_ID', '=', 'CAT_STATION.OBL_ID')
                    ->join('TOS_MES', 'CAT_STATION.IND_ST', '=', 'TOS_MES.IND_ST') // TOS_MES
                    ->orderBy('CAT_STATION.OBL_ID', 'asc')
                    ->orderBy('CAT_STATION.IND_ST')
                    ->where('YEAR_CH', '=',$yearFrom)
                    ->where('YEAR_CH', '<=',$yearTo)
                    ->where('MONTH_CH', '=',$monthFrom)
                    ->where('MONTH_CH','<=', $monthTo)
                    ->get();*/

       /*  $dataForTable = DB::table('TOS_MES')
                        ->select(DB::raw('c.obl_id,  c.ind_st, tm.* '))
                        ->from(DB::raw('TOS_MES tm , CAT_STATION c, CAT_OBL o'))
                        ->where(DB::raw('c.IND_ST = tm.IND_ST'))
                        ->where(DB::raw('c.OBL_ID = o.OBL_ID'))
                        ->get();*/
            $dataForTable = DB::table('TOS_MES')
                ->select('TOS_MES.T', 'TOS_MES.OS')
                ->join('CAT_OBL', 'CAT_OBL.OBL_ID', '=', 'CAT_STATION.OBL_ID')
                ->join('CAT_STATION', 'CAT_STATION.IND_ST', '=', 'TOS_MES.IND_ST')
                ->whereBetween('YEAR_CH',[$yearFrom, $yearTo])
               /* ->where('YEAR_CH', '>=', $yearTo)*/
                ->whereBetween('MONTH_CH', [$monthFrom,$monthTo])
                ->get();


                /*->join('CAT_STATION', 'TOS_MES.IND_ST', '=', 'CAT_STATION.IND_ST')
                ->join('CAT_OBL', 'CAT_OBL.OBL_ID', '=', 'CAT_STATION.OBL_ID')
                ->orderBy('TOS_MES.IND_ST', 'asc')
                ->select($this->collumns)
                ->simplePaginate(15)
                ->where('YEAR_CH', '=', $yearFrom)
                ->where('YEAR_CH', '<=', $yearTo)
                ->where('MONTH_CH', '=', $monthFrom)
                ->where('MONTH_CH', '<=', $monthTo)
                ->get();*/

            /*$kndaily = new DateMonth($dataForTable, ['dateFrom' => $dateFrom, 'dateTo' => $dateTo], $this->categories);
            $tmp = $kndaily->calculate();*/
           /* $dataForTable = collect($tmp);*/

            /**
             * Create pagination links
             */
/*            $countPages = ceil(count($dataForTable) / PER_PAGE);
            $paginationLinks = $helper->generateLinksForPagination(url('/dateformonth'), $countPages, $currentPage, true);*/

            /**
             * array with all data for view
             */
            // склеиваем массив в строку
               /* $selfTo = array_merge($monthTo, $yearTo);
                $dateTo = implode(',', $selfTo);
                $selfFrom = array_merge($monthFrom, $yearFrom);
                $dateFrom = implode(',', $selfFrom);*/
            $data = [
                'regions' => $regions->getAllRegions(),
                'stations' => $stations->getAllStation(),
                'categories' => $this->categories,
               // 'dataForTable' => $dataForTable->forPage($currentPage, PER_PAGE),
                'selectedCategories' => $selectedCategories,
              //  'paginationLinks' => $paginationLinks,
                'dateTo' => $dateTo,
                'dateFrom' => $dateFrom
            ];
        }

        return view('/site.dateformonth.kode_dateformonth', $data);
    }

    public function getDataKodeKN()
    {
        $helper = new Helper();
        $stations = new Station();

        parse_str($_POST['data'], $data);

        $ajaxIdentification = $data['requestName'];

        switch ($ajaxIdentification) {
            case "selectStation":
                {
                    if (empty($data['regionName']))
                        {
                        $stations = $stations->getAllStation();
                        }
                    else
                        {
                        $stations->regionName = $data['regionName'];
                        $stations = $stations->filterStation();
                        }

                    $data =
                        [
                        'station' => $stations
                        ];

                    $response_data = json_encode($data);
                    return response($response_data, 200);
                    break;
                }

            case "selectInfoForTable":
                {
                    $currentDate = date('m-Y');
                    $currentPage = isset($data['page']) ? $data['page'] : 1;

                    //add default category
                    $defaultColl = ['NAME_OBL', 'NAME_ST', 'IND_ST', 'DATE_CH'];
                    if (isset($data['collumns']))
                    {
                        $collumns = $data['collumns'];
                    } else $collumns = [];

                    $helper->addItemsinArr($defaultColl, $collumns);

                    //Выборка категорий по выбраным пользователем колонкам
                    foreach ($this->categories as $category) {
                        foreach ($collumns as $collumn) {
                            if ($category['code_col_name'] == $collumn) {
                                $categories[] = $category;
                            }
                        }
                    }

                    $dateFrom = date('m-Y', (strtotime($data['dateFrom']) - (60 * 60 * 24)));
                        $dateFrom = explode(".", $dateFrom);
                        $monthFrom = array_shift($dateFrom);
                        $yearFrom = array_pop($dateFrom);
                    $dateTo = $data['dateTo'];
                    if ($data['dateTo'] == $currentDate) {
                        if ($data['dateFrom'] == $data['dateTo']) {
                            $dateFrom = date('m-Y', (strtotime($data['dateFrom']) - ((60 * 60 * 24) * 2)));
                                $dateFrom = explode(".", $dateFrom);
                                $monthFrom = array_shift($dateFrom);
                                $yearFrom = array_pop($dateFrom);
                        }
                        $dateTo = date('m-Y', (strtotime($data['dateTo']) - (60 * 60 * 24)));
                            $dateTo = explode(".", $dateTo);
                            $monthTo = array_shift($dateTo);
                            $yearTo = array_pop($dateTo);
                    }

                    /**
                     * Save selected filters
                     */
                    $uId = Auth::getUser()->getAuthIdentifier();
                    if (DB::table('user_categories')->where('user_id', $uId)->where('page', 'dateformonth')->exists()) {
                        DB::table('user_categories')->where('user_id', $uId)->where('page', 'dateformonth')->update(
                            ['categories_list' => $_POST['data']]
                        );
                    } else {
                        DB::table('user_categories')->where('user_id', $uId)->insert(
                            ['user_id' => $uId, 'page' => 'dateformonth', 'categories_list' => $_POST['data']]
                        );
                    }


                    /**
                     * Data filtering
                     */
                    if (isset($data['regionName']) && empty($data['stationName'])) {
                        $dataForTable = DB::table('CAT_STATION')
                            ->join('CAT_OBL', 'CAT_STATION.OBL_ID', '=', 'CAT_OBL.OBL_ID')
                            ->join('TOS_MES', 'CAT_STATION.IND_ST', '=', 'TOS_MES.IND_ST')
                            ->orderBy('CAT_STATION.OBL_ID', 'asc')
                            ->orderBy('CAT_STATION.IND_ST')
                            ->whereIn('CAT_STATION.OBL_ID', $data['regionName'])
                            ->whereBetween('YEAR_CH', [$yearFrom, $yearTo])
                            ->whereBetween('MONTH_CH', [$monthFrom, $monthTo])
                            ->get();

                    } else if (isset($data['regionName']) && isset($data['stationName'])) {
                        $dataForTable = DB::table('CAT_STATION')
                            ->join('CAT_OBL', 'CAT_STATION.OBL_ID', '=', 'CAT_OBL.OBL_ID')
                            ->join('TOS_MES', 'CAT_STATION.IND_ST', '=', 'TOS_MES.IND_ST')
                            ->orderBy('CAT_STATION.OBL_ID', 'asc')
                            ->orderBy('CAT_STATION.IND_ST')
                            ->whereIn('CAT_STATION.OBL_ID', $data['regionName'])
                            ->whereIn('CAT_STATION.IND_ST', $data['stationName'])
                            ->whereBetween('YEAR_CH', [$yearFrom, $yearTo])
                            ->whereBetween('MONTH_CH', [$monthFrom, $monthTo])
                            ->get();

                    } else if (empty($data['regionName']) && isset($data['stationName'])) {
                        $dataForTable = DB::table('CAT_STATION')
                            ->join('CAT_OBL', 'CAT_STATION.OBL_ID', '=', 'CAT_OBL.OBL_ID')
                            ->join('TOS_MES', 'CAT_STATION.IND_ST', '=', 'TOS_MES.IND_ST')
                            ->orderBy('CAT_STATION.OBL_ID', 'asc')
                            ->orderBy('CAT_STATION.IND_ST')
                            ->whereIn('CAT_STATION.IND_ST', $data['stationName'])
                            ->whereBetween('YEAR_CH', [$yearFrom, $yearTo])
                            ->whereBetween('MONTH_CH', [$monthFrom, $monthTo])
                            ->get();

                    } else if (empty($data['regionName']) && empty($data['stationName'])) {
                        $dataForTable = DB::table('CAT_STATION')
                            ->join('CAT_OBL', 'CAT_STATION.OBL_ID', '=', 'CAT_OBL.OBL_ID')
                            ->join('TOS_MES', 'CAT_STATION.IND_ST', '=', 'TOS_MES.IND_ST')
                            ->orderBy('CAT_STATION.OBL_ID', 'asc')
                            ->orderBy('CAT_STATION.IND_ST')
                            ->whereBetween('YEAR_CH', [$yearFrom, $yearTo])
                            ->whereBetween('MONTH_CH', [$monthFrom, $monthTo])
                            ->get();
                    }

                    if ($data['dateFrom'] == $currentDate)
                        {
                        $dateFrom = date('m-Y', (strtotime($data['dateFrom']) - (60 * 60 * 24)));
                        }
                    else
                        {
                        $dateFrom = $data['dateFrom'];
                        }
                   /* $kndaily = new DateMonth($dataForTable, ['dateFrom' => $dateFrom, 'dateTo' => $dateTo], $categories);
                    $tmp = $kndaily->calculate();*/

                   // $dataForTable = collect($tmp);

                    $countStr = count($dataForTable);
                    $countPages = ceil($countStr / PER_PAGE);

                    $paginationLinks = $countPages > 1 ? $helper->generateLinksForPagination(url('/dateformonth'), $countPages, $currentPage, true) : "";

                    $dataOut = [
                        'categories' => $this->categories,
                        'selectedCategories' => $categories,
                        'dataForTable' => $dataForTable->forPage($currentPage, PER_PAGE),
                        'countPages' => $countPages,
                        'currentPage' => $currentPage,
                        'paginationLinks' => $paginationLinks,
                    ];

                    return view('/site.dateformonth.table', $dataOut);
                    break;
                }
        }
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\{
    Region, Station, UserCategory, TOS_MES
};

use App\Helpers\{
    Helper
};

use Auth;

use DB;




class DateForMonthController extends Controller
{
    private $categories = [
        [
            'col_name' => 'Назва області',
            'short_col_name' => 'Назва області',
            'code_col_name' => 'NAME_OBL'
        ],
        [
            'col_name' => 'Назва станції',
            'short_col_name' => 'Назва станції',
            'code_col_name' => 'NAME_ST'
        ],
        [
            'col_name' => 'Індекс',
            'short_col_name' => 'Індекс',
            'code_col_name' => 'IND_ST'
        ],
        [
            'col_name' => 'Рік',
            'short_col_name' => 'Рік',
            'code_col_name' => 'YEAR_CH'
        ],
        [
            'col_name' => 'Місяць',
            'short_col_name' => 'Місяць',
            'code_col_name' => 'MONTH_CH'
        ],
        [
            'col_name' => 'Середня Т',
            'short_col_name' => 'Середня Т',
            'code_col_name' => 'T'
        ],
        [
            'col_name' => 'Сума опадів',
            'short_col_name' => 'Сума опадів',
            'code_col_name' => 'OS'
        ],
    ];

    private $collumns = ['NAME_OBL', 'NAME_ST', 'IND_ST', 'YEAR_CH', 'MONTH_CH', 'T', 'OS'];

    public function __construct()
    {
        $this->middleware('auth');
        define("PER_PAGE", 18);
    }

    public function show()
    {
        $regions = new Region();
        $stations = new Station();
        $helper = new Helper();

        /*$currentDate = date('Y-n');

        $uId = Auth::getUser()->getAuthIdentifier();

        if (UserCategory::where('user_id', $uId)->where('page', 'dateformonth')->exists()) {
            $currentDate = date('Y-n');

            $selectedFilters = UserCategory::all()->where('user_id', '=', $uId)->where('page', 'dateformonth')->first();
            parse_str($selectedFilters->categories_list, $selectedFilters);

            $selectedRegions = isset($selectedFilters['regionName']) ? $selectedFilters['regionName'] : null;
            $selectedStations = isset($selectedFilters['stationName']) ? $selectedFilters['stationName'] : null;

            //add default category
            $defaultColl = ['NAME_OBL', 'NAME_ST', 'IND_ST'];
            if (isset($selectedFilters['collumns'])) {
                $collumns = $selectedFilters['collumns'];
            } else $collumns = [];

            //Выборка категорий по выбраным пользователем колонкам
            foreach ($this->categories as $category) {
                foreach ($collumns as $collumn) {
                    if ($category['code_col_name'] == $collumn) {
                        $categories[] = $category;
                    }
                }
            }

            $dateFrom = date('Y-n', (strtotime($selectedFilters['dateFrom']) - (60 * 60 * 24)));
            $dateTo = $selectedFilters['dateTo'];
            if ($selectedFilters['dateTo'] == $currentDate) {
                if ($selectedFilters['dateFrom'] == $selectedFilters['dateTo']) {
                    $dateFrom = date('Y-n', (strtotime($selectedFilters['dateFrom']) - ((60 * 60 * 24) * 2)));
                }
                $dateTo = date('Y-n', (strtotime($selectedFilters['dateTo']) - (60 * 60 * 24)));
            }

            /*  $dataForTable = TOS_MES::with('Region','Station')->where('YEAR_CH', '=', $yearFrom)->where('YEAR_CH', '=', $yearTo)
                  ->where('MONTH_CH', '=', $monthFrom)->where('MONTH_CH', '=', $monthTo)
                  ->get();

            if ($selectedFilters['dateFrom'] == $currentDate) {
                $dateFrom = date('Y-n', (strtotime($selectedFilters['dateFrom']) - (60 * 60 * 24)));
            } else {
                $dateFrom = $selectedFilters['dateFrom'];
            }

           /* $dataForTable = Station::region()->tos_mes();

           $dataForTable = DB::table('CAT_STATION')
                ->join('CAT_OBL', 'CAT_STATION.OBL_ID', '=', 'CAT_OBL.OBL_ID')
                ->join('TOS_MES', 'CAT_STATION.IND_ST', '=', 'TOS_MES.IND_ST')
                ->select('CAT_OBL.NAME_OBL', 'CAT_STATION.NAME_ST', 'TOS_MES.IND_ST', 'TOS_MES.YEAR_CH', 'TOS_MES.MONTH_CH', 'TOS_MES.T', 'TOS_MES.OS')
                ->orderBy('CAT_STATION.OBL_ID', 'asc')
                ->orderBy('CAT_STATION.IND_ST')
                ->get();

            /* select c.obl_id,  c.ind_st, tm.*
  from TOS_MES tm, CAT_STATION c, CAT_OBL o  where ((c.ind_st = tm.ind_st) and ( c.obl_id=o.obl_id))


            $currentPage = isset($_GET['page']) ? $_GET['page'] : 1;
            $countStr = count($dataForTable);
            $countPages = ceil($countStr / PER_PAGE);
            $paginationLinks = $countPages > 1 ? $helper->generateLinksForPagination(url('/dateformonth'), $countPages, $currentPage, true) : "";

            $data = [
                'regions' => $regions->getAllRegions(),
                'stations' => $stations->getAllStation(),
                'categories' => $this->categories,
                'selectedCategories' => $this->categories,
                'dataForTable' => $dataForTable->forPage($currentPage, PER_PAGE),
                'currentDate' => $currentDate,
                'currentPage' => $currentPage,
                'paginationLinks' => $paginationLinks,
                'dateFrom' => $dateFrom,
                'dateTo' => $dateTo,
            ];
            return view('site.dateformonth.kode_dateformonth', $data);
        }*/


        /* $dataForTable = Station::region()->tos_mes();*/

        $dataForTable = DB::table('CAT_STATION')
            ->join('CAT_OBL', 'CAT_STATION.OBL_ID', '=', 'CAT_OBL.OBL_ID')
            ->join('TOS_MES', 'CAT_STATION.IND_ST', '=', 'TOS_MES.IND_ST')
            ->select('CAT_OBL.NAME_OBL', 'CAT_STATION.NAME_ST', 'TOS_MES.IND_ST', 'TOS_MES.YEAR_CH', 'TOS_MES.MONTH_CH', 'TOS_MES.T', 'TOS_MES.OS')
            ->orderBy('CAT_STATION.OBL_ID', 'asc')
            ->orderBy('CAT_STATION.IND_ST')
            ->get();

        $currentPage = isset($_GET['page']) ? $_GET['page'] : 1;
        $countStr = count($dataForTable);
        $countPages = ceil($countStr / PER_PAGE);
        $paginationLinks = $countPages > 1 ? $helper->generateLinksForPagination(url('/dateformonth'), $countPages, $currentPage, true) : "";

        $dateFrom = date('Y-n');
        $dateTo = date('Y-n');


        $data = [
            'regions' => $regions->getAllRegions(),
            'stations' => $stations->getAllStation(),
            'categories' => $this->categories,
            'selectedCategories' => $this->categories,
            'dataForTable' => $dataForTable->forPage($currentPage, PER_PAGE),
            'currentPage' => $currentPage,
            'paginationLinks' => $paginationLinks,
            'dateFrom' => $dateFrom,
            'dateTo' => $dateTo,
        ];
        return view('site.dateformonth.kode_dateformonth', $data);
    }


    /*
     $dateFrom = explode(".", $dateFrom);
     $monthFrom = array_shift($dateFrom);
     $yearFrom = array_pop($dateFrom);

     $dateTo = explode(".", $dateTo);
     $monthTo = array_shift($dateTo);
     $yearTo = array_pop($dateTo);
    */


   public function getData(Request $request)
    {

        $helper = new Helper();
        $stations = new Station();

        parse_str($_POST['data'], $data);

        $ajaxIdentification = $data['requestName'];

        switch ($ajaxIdentification) {
            case "selectStation":
                {
                    if (empty($data['regionName'])) {
                        $stations = $stations->getAllStation();

                    } else {
                        $stations->regionName = $data['regionName'];
                        $stations = $stations->filterStation();
                    }

                    $data = [
                        'station' => $stations
                    ];

                    $response_data = json_encode($data);
                    return response($response_data, 200);
                    break;
                }

            case "selectInfoForTable":
                {
                    $currentDate = date('Y-n');
                    if ($data['dateFrom'] == $currentDate) {
                        $dateFrom = date('Y-n', (strtotime($data['dateFrom']) - (60 * 60 * 24)));
                    } else {
                        $dateFrom = $data['dateFrom'];
                    }

                    $currentPage = isset($data['page']) ? $data['page'] : 1;

                    //add default category
                    $defaultColl = ['NAME_OBL', 'NAME_ST', 'IND_ST'];
                    if (isset($data['collumns'])) {
                        $collumns = $data['collumns'];
                    } else $collumns = [];

                    //Выборка категорий по выбраным пользователем колонкам
                    foreach ($this->categories as $category) {
                        foreach ($collumns as $collumn) {
                            if ($category['code_col_name'] == $collumn) {
                                $categories[] = $category;
                            }
                        }
                    }

                    $dateFrom = date('Y-n', (strtotime($data['dateFrom']) - (60 * 60 * 24)));
                    $dateTo = $data['dateTo'];

                    if ($data['dateTo'] == $currentDate) {
                        if ($data['dateFrom'] == $data['dateTo']) {
                            $dateFrom = date('Y-n', (strtotime($data['dateFrom']) - ((60 * 60 * 24) * 2)));
                        }
                        $dateTo = date('Y-n', (strtotime($data['dateTo']) - (60 * 60 * 24)));
                    }

                    /*Save selected filters*/

                    $uId = Auth::getUser()->getAuthIdentifier();
                    if (DB::table('user_categories')->where('user_id', $uId)->where('page', 'dateformonth')->exists()) {
                        DB::table('user_categories')->where('user_id', $uId)->where('page', 'dateformonth')->update(
                            ['categories_list' => $_POST['data']]
                        );
                    } else {
                        DB::table('user_categories')->where('user_id', $uId)->insert(
                            ['user_id' => $uId, 'page' => 'dateformonth', 'categories_list' => $_POST['data']]
                        );
                    }

                    /*Разбиваю строку в массив и получаю 1-й и последний элемент массива*/


                      $dataForTable = DB::table('CAT_STATION')
                          ->join('CAT_OBL', 'CAT_STATION.OBL_ID', '=', 'CAT_OBL.OBL_ID')
                          ->join('TOS_MES', 'CAT_STATION.IND_ST', '=', 'TOS_MES.IND_ST')
                          ->select('CAT_OBL.NAME_OBL', 'CAT_STATION.NAME_ST', 'TOS_MES.IND_ST', 'TOS_MES.YEAR_CH', 'TOS_MES.MONTH_CH', 'TOS_MES.T', 'TOS_MES.OS')
                          ->orderBy('CAT_STATION.OBL_ID', 'asc')
                          ->orderBy('CAT_STATION.IND_ST')
                          ->get();


                    /*                    $dateFrom = $request->input('dateFrom');
                                        $dateTo = $request->input('dateTo');*/

                /*    $dateFrom = explode(".", $dateFrom);
                    $monthFrom = array_shift($dateFrom);
                    $yearFrom = array_pop($dateFrom);

                    $dateTo = explode(".", $dateTo);
                    $monthTo = array_shift($dateTo);
                    $yearTo = array_pop($dateTo);*/


                    $currentPage = isset($data['page']) ? $data['page'] : 1;

                  /*  $dataForTable = Station::where('YEAR_CH', '=', $yearFrom)
                        ->where('YEAR_CH', '<=', $yearTo)
                        ->where('MONTH_CH', '=', $monthTo)
                        ->where('MONTH_CH', '<=', $monthFrom)
                        ->region()
                        ->tos_mes();*/





                    $countStr = count($dataForTable);
                    $countPages = ceil($countStr / PER_PAGE);

                    $paginationLinks = $countPages > 1 ? $helper->generateLinksForPagination(url('/dateformonth'), $countPages, $currentPage, true) : "";

                    $dataForTable = [
                        'categories' => $this->categories,
                        'selectedCategories' => $this->categories,
                        'dataForTable' => $dataForTable->forPage($currentPage, PER_PAGE),
                        'countPages' => $countPages,
                        'currentPage' => $currentPage,
                        'paginationLinks' => $paginationLinks,
                    ];

                    return view('site.dateformonth.table', $dataForTable);

                }
        }
    }
}
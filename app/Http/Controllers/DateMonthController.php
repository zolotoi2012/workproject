<?php

namespace App\Http\Controllers;

use App\Helpers\{
    Helper
};
use App\{
    Region, Station, UserCategory, TOS_MES
};

use DB;
use Auth;


class DateMonthController extends Controller
{
    private $categories = [
        [
            'col_name' => 'Назва області',
            'short_col_name' => 'Назва області',
            'code_col_name' => 'NAME_OBL'
        ],
        [
            'col_name' => 'Назва станції',
            'short_col_name' => 'Назва станції',
            'code_col_name' => 'NAME_ST'
        ],
        [
            'col_name' => 'Індекс',
            'short_col_name' => 'Індекс',
            'code_col_name' => 'IND_ST'
        ],
        [
            'col_name' => 'Рік',
            'short_col_name' => 'Рік',
            'code_col_name' => 'YEAR_CH'
        ],
        [
            'col_name' => 'Місяць',
            'short_col_name' => 'Місяць',
            'code_col_name' => 'MONTH_CH'
        ],
        [
            'col_name' => 'Середня Т',
            'short_col_name' => 'Середня Т',
            'code_col_name' => 'T'
        ],
        [
            'col_name' => 'Сума опадів',
            'short_col_name' => 'Сума опадів',
            'code_col_name' => 'OS'
        ],
    ];

    private $collumns = ['NAME_OBL', 'NAME_ST', 'IND_ST', 'YEAR_CH', 'MONTH_CH', 'T', 'OS'];

    /**
     * Kode_knController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
        define("PER_PAGE", 18);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show()
    {

        $helper = new Helper();
        $regions = new Region();
        $stations = new Station();

        $uId = Auth::getUser()->getAuthIdentifier();

        if(DB::table('user_categories')->where('user_id', $uId)->where('page', 'content_kode_kn_dateformonth')->exists())
        {
            $currentDate = date('Y-n');
            $currentPage = isset($_GET['page']) ? $_GET['page'] : 1;

            $selectedFilters = UserCategory::all()->where('user_id', '=', $uId)->where('page', 'content_kode_kn_dateformonth')->first();
            parse_str($selectedFilters->categories_list, $selectedFilters);

            $selectedRegions = isset($selectedFilters['regionName']) ? $selectedFilters['regionName'] : null;
            $selectedStations = isset($selectedFilters['stationName']) ? $selectedFilters['stationName'] : null;

            //add default category
            $defaultColl = ['NAME_OBL', 'NAME_ST', 'IND_ST', 'YEAR_CH', 'MONTH_CH', 'T', 'OS'];
            if (isset($selectedFilters['collumns'])) {
                $collumns = $selectedFilters['collumns'];
            } else $collumns = [];

           $helper->addItemsinArr($defaultColl, $collumns);

            //Выборка категорий по выбраным пользователем колонкам
            foreach ($this->categories as $category) {
                foreach ($collumns as $collumn) {
                    if ($category['code_col_name'] == $collumn) {
                        $categories[] = $category;
                    }
                }
            }

            $dateFrom = date('Y-n', (strtotime($selectedFilters['dateFrom']) - (60 * 60 * 24)));
            $dateTo = $selectedFilters['dateTo'];
            if ($selectedFilters['dateTo'] == $currentDate) {
                if ($selectedFilters['dateFrom'] == $selectedFilters['dateTo']) {
                    $dateFrom = date('Y-n', (strtotime($selectedFilters['dateFrom']) - ((60 * 60 * 24) * 2)));
                }
                $dateTo = date('Y-n', (strtotime($selectedFilters['dateTo']) - (60 * 60 * 24)));
            }

            /**
             * Data filtering
             */
            if (isset($selectedFilters['regionName']) && empty($selectedFilters['stationName'])) {
                $dataForTable = DB::table('CAT_STATION')
                    ->join('CAT_OBL', 'CAT_STATION.OBL_ID', '=', 'CAT_OBL.OBL_ID')
                    ->join('TOS_MES', 'CAT_STATION.IND_ST', '=', 'TOS_MES.IND_ST')
                    ->select('CAT_OBL.NAME_OBL', 'CAT_STATION.NAME_ST', 'TOS_MES.IND_ST', 'TOS_MES.YEAR_CH', 'TOS_MES.MONTH_CH', 'TOS_MES.T', 'TOS_MES.OS')
                    ->orderBy('CAT_STATION.OBL_ID', 'asc')
                    ->orderBy('CAT_STATION.IND_ST')
                    ->whereIn('CAT_STATION.OBL_ID', $selectedFilters['regionName'])
                    ->get();

            } else if (isset($selectedFilters['regionName']) && isset($selectedFilters['stationName'])) {
                $dataForTable = DB::table('CAT_STATION')
                    ->join('CAT_OBL', 'CAT_STATION.OBL_ID', '=', 'CAT_OBL.OBL_ID')
                    ->join('TOS_MES', 'CAT_STATION.IND_ST', '=', 'TOS_MES.IND_ST')
                    ->select('CAT_OBL.NAME_OBL', 'CAT_STATION.NAME_ST', 'TOS_MES.IND_ST', 'TOS_MES.YEAR_CH', 'TOS_MES.MONTH_CH', 'TOS_MES.T', 'TOS_MES.OS')
                    ->orderBy('CAT_STATION.OBL_ID', 'asc')
                    ->orderBy('CAT_STATION.IND_ST')
                    ->whereIn('CAT_STATION.OBL_ID', $selectedFilters['regionName'])
                    ->whereIn('CAT_STATION.IND_ST', $selectedFilters['stationName'])
                    ->get();

            } else if (empty($selectedFilters['regionName']) && isset($selectedFilters['stationName'])) {
                $dataForTable = DB::table('CAT_STATION')
                    ->join('CAT_OBL', 'CAT_STATION.OBL_ID', '=', 'CAT_OBL.OBL_ID')
                    ->join('TOS_MES', 'CAT_STATION.IND_ST', '=', 'TOS_MES.IND_ST')
                    ->select('CAT_OBL.NAME_OBL', 'CAT_STATION.NAME_ST', 'TOS_MES.IND_ST', 'TOS_MES.YEAR_CH', 'TOS_MES.MONTH_CH', 'TOS_MES.T', 'TOS_MES.OS')
                    ->orderBy('CAT_STATION.OBL_ID', 'asc')
                    ->whereIn('CAT_STATION.IND_ST', $selectedFilters['stationName'])
                    ->orderBy('CAT_STATION.IND_ST')
                    ->get();

            } else if (empty($selectedFilters['regionName']) && empty($selectedFilters['stationName'])) {
                $dataForTable = DB::table('CAT_STATION')
                    ->join('CAT_OBL', 'CAT_STATION.OBL_ID', '=', 'CAT_OBL.OBL_ID')
                    ->join('TOS_MES', 'CAT_STATION.IND_ST', '=', 'TOS_MES.IND_ST')
                    ->select('CAT_OBL.NAME_OBL', 'CAT_STATION.NAME_ST', 'TOS_MES.IND_ST', 'TOS_MES.YEAR_CH', 'TOS_MES.MONTH_CH', 'TOS_MES.T', 'TOS_MES.OS')
                    ->orderBy('CAT_STATION.OBL_ID', 'asc')
                    ->orderBy('CAT_STATION.IND_ST')
                    ->get();
            }

            if ($selectedFilters['dateFrom'] == $currentDate) {
                $dateFrom = date('Y-n', (strtotime($selectedFilters['dateFrom']) - (60 * 60 * 24)));
            } else {
                $dateFrom = $selectedFilters['dateFrom'];
            }

            $countStr = count($dataForTable);
            $countPages = ceil($countStr / PER_PAGE);

            $paginationLinks = $countPages > 1 ? $helper->generateLinksForPagination(url('/dateformonth'), $countPages, $currentPage, true) : "";

            /**
             * array with all data for view
             */
            $data = [
                'regions' => $regions->getAllRegions(),
                'stations' => $stations->getAllStation(),
                'categories' => $this->categories,
                'selectedCategories' => $categories,
                'dataForTable' => $dataForTable->forPage($currentPage, PER_PAGE),
                'selectedRegions' => $selectedRegions,
                'selectedStations' => $selectedStations,
                'paginationLinks' => $paginationLinks,
                'dateTo' => $dateTo,
                'dateFrom' => $dateFrom
            ];

        } else {
            //if first auth
            $currentDate = Date('Y-n');
            $dateFrom = date('Y-n', (strtotime($currentDate) - (60 * 60 * 24) * 2));
            $dateTo = date('Y-n', (strtotime($currentDate) - (60 * 60 * 24)));

            $selectedCategories = $this->categories;
            $currentPage = isset($_GET['page']) ? $_GET['page'] : 1;

            /**
             * Get data
             */
            $dataForTable = DB::table('CAT_STATION')
                ->join('CAT_OBL', 'CAT_STATION.OBL_ID', '=', 'CAT_OBL.OBL_ID')
                ->join('TOS_MES', 'CAT_STATION.IND_ST', '=', 'TOS_MES.IND_ST')
                ->select('CAT_OBL.NAME_OBL', 'CAT_STATION.NAME_ST', 'TOS_MES.IND_ST', 'TOS_MES.YEAR_CH', 'TOS_MES.MONTH_CH', 'TOS_MES.T', 'TOS_MES.OS')
                ->orderBy('CAT_STATION.OBL_ID', 'asc')
                ->orderBy('CAT_STATION.IND_ST')
                ->get();

            /**
             * Create pagination links
             */
            $countPages = ceil(count($dataForTable) / PER_PAGE);
            $paginationLinks = $helper->generateLinksForPagination(url('/dateformonth'), $countPages, $currentPage, true);

            /**
             * array with all data for view
             */
            $data = [
                'regions' => $regions->getAllRegions(),
                'stations' => $stations->getAllStation(),
                'categories' => $this->categories,
                'dataForTable' => $dataForTable->forPage($currentPage, PER_PAGE),
                'selectedCategories' => $selectedCategories,
                'paginationLinks' => $paginationLinks,
                'dateTo' => $dateTo,
                'dateFrom' => $dateFrom
            ];
        }

        return view('/site.dateformonth.kode_dateformonth', $data);
    }



    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Contracts\View\Factory|\Illuminate\View\View|\Symfony\Component\HttpFoundation\Response
     */
    public function getData()
    {
        $helper = new Helper();
        $stations = new Station();

        parse_str($_POST['data'], $data);

        $ajaxIdentification = $data['requestName'];

        switch ($ajaxIdentification) {
            case "selectStation":
                {
                    if (empty($data['regionName'])) {
                        $stations = $stations->getAllStation();

                    } else {
                        $stations->regionName = $data['regionName'];
                        $stations = $stations->filterStation();
                    }

                    $data = [
                        'station' => $stations
                    ];

                    $response_data = json_decode($data, true);
                    return response($response_data, 200);
                    break;
                }

            case "selectInfoForTable":
                {
                    $currentPage = isset($data['page']) ? $data['page'] : 1;

                    //add default category
                    $defaultColl = ['NAME_OBL', 'NAME_ST', 'IND_ST', 'YEAR_CH', 'MONTH_CH', 'T', 'OS'];
                    if (isset($data['collumns'])) {
                        $collumns = $data['collumns'];
                    } else $collumns = [];

                    $helper->addItemsinArr($defaultColl, $collumns);

                    //Выборка категорий по выбраным пользователем колонкам
                    foreach ($this->categories as $category) {
                        foreach ($collumns as $collumn) {
                            if ($category['code_col_name'] == $collumn) {
                                $categories[] = $category;
                            }
                        }
                    }

                    $uId = Auth::getUser()->getAuthIdentifier();
                    if (DB::table('user_categories')->where('user_id', $uId)->where('page', 'kodedateformonth')->exists())
                    {
                        DB::table('user_categories')->where('user_id', $uId)->where('page', 'kodedateformonth')->update(
                            ['categories_list' => $_POST['data']]
                        );
                    }
                    else
                    {
                        DB::table('user_categories')->where('user_id', $uId)->insert(
                            ['user_id' => $uId, 'page' => 'kodedateformonth', 'categories_list' => $_POST['data']]
                        );
                    }


                   /*
                   Дата из инпутов приходит в формате Y-m, то есть с ведущим месяцем,
                   а данные в таблице в формате int и без ведущего месяца
                   Из-за чего приходиться удалять 0 из получаемых данных от input
                    */
                    $dateFrom = $data['dateFrom'];
                    $dateTo = $data['dateTo'];

                    $dateFrom = explode('-', $dateFrom);
                    $dateTo = explode('-', $dateTo);

                    // занесение 5-го елемента строки input в переменную
                    /*$zero = substr($dateFrom, 5, 1);
                    $ziro = substr($dateTo, 5, 1);

                    // проверка: является ли 5-й елемент 0 (2018-01 \ 2018-11)
                    if($zero === '0' ) {
                        $dateFrom = substr_replace($dateFrom, '', 5, 1);
                    }

                    if($ziro === '0') {
                        $dateTo = substr_replace($dateTo, '', 5, 1);
                    }*/

                    $zeroFrom = substr($dateFrom[1], 0, 1);
                    $zeroTo = substr($dateTo[1], 0, 1);

                    // проверка: является ли 5-й елемент 0 (2018-01 \ 2018-11)
                    if($zeroFrom === '0' ) {
                        $dateFrom[1] = substr_replace($dateFrom[1], '', 0, 1);
                    }

                    if($zeroTo === '0') {
                        $dateTo[1] = substr_replace($dateTo[1], '', 0, 1);
                    }


                   /**
                     * Data filtering
                    */

                    if (isset($data['regionName']) && empty($data['stationName']))
                    {
                        $dataForTable = DB::table('CAT_STATION')
                            ->join( 'CAT_OBL', 'CAT_STATION.OBL_ID', '=', 'CAT_OBL.OBL_ID')
                            ->join('TOS_MES', 'CAT_STATION.IND_ST', '=', 'TOS_MES.IND_ST')
                            /*->where(DB::raw('DATE_FORMAT("%Y-%c", CONCAT(YEAR_CH, "-", MONTH_CH))'), "<=", DB::raw('DATE_FORMAT("%Y-%c", "$dateFrom")'))
                            ->where(DB::raw('DATE_FORMAT("%Y-%c", CONCAT(YEAR_CH, "-", MONTH_CH))'), ">=", DB::raw('DATE_FORMAT("%Y-%c", "$dateTo")'))*/
                            /*->whereRaw(' DATE_FORMAT(CONCAT(YEAR_CH, "-", MONTH_CH), "%Y-%c") BETWEEN DATE_FORMAT("$dateFrom", "%Y-%c") AND DATE_FORMAT("$dateTo", "%Y-%c") ')*/
                            /*->whereRaw(' DATE_FORMAT(CONCAT(YEAR_CH, "-", MONTH_CH), "%Y-%c") >= DATE_FORMAT('.$dateFrom.', "%Y-%c")
                            AND DATE_FORMAT(CONCAT(YEAR_CH, "-", MONTH_CH), "%Y-%c") <= DATE_FORMAT('.$dateTo.', "%Y-%c") ')*/
                            ->whereBetween('YEAR_CH', [$dateFrom[0], $dateTo[0]])
                            ->whereBetween('MONTH_CH', [$dateFrom[1], $dateTo[1]])
                            /*[DB::raw('DATE_FORMAT('.$dateFrom.', "%Y-%c")'), DB::raw('DATE_FORMAT('.$dateTo.', "%Y-%c")')]*/
                            ->whereIn('CAT_STATION.OBL_ID', $data['regionName'])
                            ->orderBy('CAT_STATION.OBL_ID', 'asc')
                            ->orderBy('CAT_STATION.IND_ST')
                            /*->where(DB::raw('CONCAT(YEAR_CH, "-", MONTH_CH)'), "<=", $dateTo)
                            ->whereRaw('CONCAT(YEAR_CH, "-", MONTH_CH) BETWEEN '.$dateFrom.' AND '.$dateTo.' ')*/
                            ->get();
                    }

                    else if (isset($data['regionName']) && isset($data['stationName'])) {
                        $dataForTable = DB::table('CAT_STATION')
                            ->join('CAT_OBL', 'CAT_STATION.OBL_ID', '=', 'CAT_OBL.OBL_ID')
                            ->join('TOS_MES', 'CAT_STATION.IND_ST', '=', 'TOS_MES.IND_ST')
                            ->whereBetween('YEAR_CH', [$dateFrom[0], $dateTo[0]])
                            ->whereBetween('MONTH_CH', [$dateFrom[1], $dateTo[1]])
                            ->whereIn('CAT_STATION.OBL_ID', $data['regionName'])
                            ->whereIn('CAT_STATION.IND_ST', $data['stationName'])
                            ->orderBy('CAT_STATION.OBL_ID', 'asc')
                            ->orderBy('CAT_STATION.IND_ST')
                         /*   ->whereRaw(' DATE_FORMAT( \'%Y-%m\'), $dateFrom ) <= DATE_FORMAT( (\'%Y-%m\'), CONCAT(YEAR_CH, '-', MONTH_CH))
                            AND DATE_FORMAT(\'%Y-%m\'), $dateTo) >= DATE_FORMAT(CONCAT(YEAR_CH, '-', MONTH_CH)) ')*/
                            /*->whereRaw(' DATE_FORMAT(("%Y-%c"), \'$dateFrom\') <= DATE_FORMAT("%Y-%c", CONCAT(YEAR_CH, "-", MONTH_CH))
                                             AND DATE_FORMAT(("%Y-%c"), \'$dateTo\') >= DATE_FORMAT("%Y-%c", CONCAT(YEAR_CH, "-", MONTH_CH))')*/
                          /*->whereBetween(DB::raw('DATE_FORMAT("%Y-%c", CONCAT(YEAR_CH, "-", MONTH_CH)'), [$dateFrom, $dateTo])*/
                            ->get();
                    }
                     /*
                      * $dateFrom = date("Y-n");
                      * $dateTo = date("Y-n");
                      * WHERE CONCAT(YEAR_CH, '-', MONTH_CH) as dateFrom
                      * DATE_FORMAT(dateFrom, "%Y-%m") <= $dateFrom AND CONCAT(YEAR_CH, '-', 'MONTH_CH') as dateTo DATE_FORMAT(dateTo, "%Y-%m") >= $dateTo
                      * */


                    else if (empty($data['regionName']) && isset($data['stationName'])) {
                        $dataForTable = DB::table('CAT_STATION')
                            ->join('CAT_OBL', 'CAT_STATION.OBL_ID', '=', 'CAT_OBL.OBL_ID')
                            ->join('TOS_MES', 'CAT_STATION.IND_ST', '=', 'TOS_MES.IND_ST')
                            ->orderBy('CAT_STATION.OBL_ID', 'asc')
                            ->orderBy('CAT_STATION.IND_ST')
                            ->whereIn('CAT_STATION.IND_ST', $data['stationName'])
                            ->whereBetween('YEAR_CH', [$dateFrom[0], $dateTo[0]])
                            ->whereBetween('MONTH_CH', [$dateFrom[1], $dateTo[1]])
                            ->get();
                    }

                    else if (empty($data['regionName']) && empty($data['stationName'])) {
                        $dataForTable = DB::table('CAT_STATION')
                            ->join('CAT_OBL', 'CAT_STATION.OBL_ID', '=', 'CAT_OBL.OBL_ID')
                            ->join('TOS_MES', 'CAT_STATION.IND_ST', '=', 'TOS_MES.IND_ST')
                            ->orderBy('CAT_STATION.OBL_ID', 'asc')
                            ->orderBy('CAT_STATION.IND_ST')
                            ->whereBetween('YEAR_CH', [$dateFrom[0], $dateTo[0]])
                            ->whereBetween('MONTH_CH', [$dateFrom[1], $dateTo[1]])
                            ->get();
                    }

                    $countStr = count($dataForTable);
                    $countPages = ceil($countStr / PER_PAGE);

                    $paginationLinks = $countPages > 1 ? $helper->generateLinksForPagination(url('/dateformonth'), $countPages, $currentPage, true) : "";

                    $dataOut =
                        [
                        'categories' => $this->categories,
                        'selectedCategories' => $categories,
                        'dataForTable' => $dataForTable->forPage($currentPage, PER_PAGE),
                        'countPages' => $countPages,
                        'currentPage' => $currentPage,
                        'paginationLinks' => $paginationLinks,
                        ];

                    return view('site.dateformonth.table', $dataOut);
                    break;
                }
        }
    }
}

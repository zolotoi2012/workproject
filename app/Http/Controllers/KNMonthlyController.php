<?php

namespace App\Http\Controllers;

use App\Helpers\{
    Helper, KNDaily
};
use App\{
    Region, Station, UserCategory
};

use DB;
use Auth;

class KNMonthlyController extends Controller
{
    /**
     * KNMonthlyController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
        define("PER_PAGE", 18);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show() {

        $helper = new Helper();
        $regions = new Region();
        $stations = new Station();

        $uId = Auth::getUser()->getAuthIdentifier();

        if (DB::table('user_categories')->where('user_id', $uId)->where('page', 'kodeKNmonthly')->exists()) {
            $currentDate = date('Y-m-d');
            $currentPage = isset($_GET['page']) ? $_GET['page'] : 1;

            $selectedFilters = UserCategory::all()->where('user_id', '=', $uId)->where('page', 'kodeKNmonthly')->first();
            parse_str($selectedFilters->categories_list, $selectedFilters);

            $selectedRegions = isset($selectedFilters['regionName']) ? $selectedFilters['regionName'] : null;
            $selectesStations = isset($selectedFilters['stationName']) ? $selectedFilters['stationName'] : null;

            //add default categiry
            $defaultColl = ['NAME_OBL', 'NAME_ST', 'IND_ST', 'DATE_CH'];
            if (isset($selectedFilters['collumns'])) {
                $collumns = $selectedFilters['collumns'];
            } else $collumns = [];

            $helper->addItemsinArr($defaultColl, $collumns);

            //Выборка категорий по выбраным пользователем колонкам
            foreach ($this->categories as $category) {
                foreach ($collumns as $collumn) {
                    if ($category['code_col_name'] == $collumn) {
                        $categories[] = $category;
                    }
                }
            }

            $dateFrom = date('Y-m-d', (strtotime($selectedFilters['dateFrom']) - (60 * 60 * 24)));
            $dateTo = $selectedFilters['dateTo'];
            if ($selectedFilters['dateTo'] == $currentDate) {
                if ($selectedFilters['dateFrom'] == $selectedFilters['dateTo']) {
                    $dateFrom = date('Y-m-d', (strtotime($selectedFilters['dateFrom']) - ((60 * 60 * 24) * 2)));
                }
                $dateTo = date('Y-m-d', (strtotime($selectedFilters['dateTo']) - (60 * 60 * 24)));
            }

            /**
             * Data filtering
             */
            if (isset($selectedFilters['regionName']) && empty($selectedFilters['stationName'])) {
                $dataForTable = DB::table('CAT_STATION')
                    ->join('CAT_OBL', 'CAT_STATION.OBL_ID', '=', 'CAT_OBL.OBL_ID')
                    ->join('srok', 'CAT_STATION.IND_ST', '=', 'srok.IND_ST')
                    ->orderBy('CAT_STATION.OBL_ID', 'asc')
                    ->orderBy('CAT_STATION.IND_ST')
                    ->whereIn('CAT_STATION.OBL_ID', $selectedFilters['regionName'])
                    ->whereBetween('DATE_CH', [$dateFrom, $dateTo])
                    ->get();

            } else if (isset($selectedFilters['regionName']) && isset($selectedFilters['stationName'])) {
                $dataForTable = DB::table('CAT_STATION')
                    ->join('CAT_OBL', 'CAT_STATION.OBL_ID', '=', 'CAT_OBL.OBL_ID')
                    ->join('srok', 'CAT_STATION.IND_ST', '=', 'srok.IND_ST')
                    ->orderBy('CAT_STATION.OBL_ID', 'asc')
                    ->orderBy('CAT_STATION.IND_ST')
                    ->whereIn('CAT_STATION.OBL_ID', $selectedFilters['regionName'])
                    ->whereIn('CAT_STATION.IND_ST', $selectedFilters['stationName'])
                    ->whereBetween('DATE_CH', [$dateFrom, $dateTo])
                    ->get();

            } else if (empty($selectedFilters['regionName']) && isset($selectedFilters['stationName'])) {
                $dataForTable = DB::table('CAT_STATION')
                    ->join('CAT_OBL', 'CAT_STATION.OBL_ID', '=', 'CAT_OBL.OBL_ID')
                    ->join('srok', 'CAT_STATION.IND_ST', '=', 'srok.IND_ST')
                    ->orderBy('CAT_STATION.OBL_ID', 'asc')
                    ->orderBy('CAT_STATION.IND_ST')
                    ->whereIn('CAT_STATION.IND_ST', $selectedFilters['stationName'])
                    ->whereBetween('DATE_CH', [$dateFrom, $dateTo])
                    ->get();

            } else if (empty($selectedFilters['regionName']) && empty($selectedFilters['stationName'])) {
                $dataForTable = DB::table('CAT_STATION')
                    ->join('CAT_OBL', 'CAT_STATION.OBL_ID', '=', 'CAT_OBL.OBL_ID')
                    ->join('srok', 'CAT_STATION.IND_ST', '=', 'srok.IND_ST')
                    ->orderBy('CAT_STATION.OBL_ID', 'asc')
                    ->orderBy('CAT_STATION.IND_ST')
                    ->whereBetween('DATE_CH', [$dateFrom, $dateTo])
                    ->get();
            }

            if ($selectedFilters['dateFrom'] == $currentDate) {
                $dateFrom = date('Y-m-d', (strtotime($selectedFilters['dateFrom']) - (60 * 60 * 24)));
            } else {
                $dateFrom = $selectedFilters['dateFrom'];
            }
            $kndaily = new KNDaily($dataForTable, ['dateFrom' => $dateFrom, 'dateTo' => $dateTo], $categories);
            $tmp = $kndaily->calculate();

            $dataForTable = collect($tmp);

            $countStr = count($dataForTable);
            $countPages = ceil($countStr / PER_PAGE);

            $paginationLinks = $countPages > 1 ? $helper->generateLinksForPagination(url('/kndaily'), $countPages, $currentPage, true) : "";

            /**
             * array with all data for view
             */
            $data = [
                'regions' => $regions->getAllRegions(),
                'stations' => $stations->getAllStation(),
                'categories' => $this->categories,
                'selectedCategories' => $categories,
                'dataForTable' => $dataForTable->forPage($currentPage, PER_PAGE),
                'selectedRegions' => $selectedRegions,
                'selectedStations' => $selectesStations,
                'paginationLinks' => $paginationLinks
            ];

        } else {
            //if first auth
            $currentDate = Date('Y-m-d');
            $dateFrom = date('Y-m-d', (strtotime($currentDate) - (60 * 60 * 24) * 2));
            $dateTo = date('Y-m-d', (strtotime($currentDate) - (60 * 60 * 24)));

//            $selectedCategories = $this->categories;
            $currentPage = isset($_GET['page']) ? $_GET['page'] : 1;

            /**
             * Get data
             */
//            $dataForTable = DB::table('CAT_STATION')
//                ->select($this->collumns)
//                ->join('CAT_OBL', 'CAT_STATION.OBL_ID', '=', 'CAT_OBL.OBL_ID')
//                ->join('srok', 'CAT_STATION.IND_ST', '=', 'srok.IND_ST')
//                ->orderBy('CAT_STATION.OBL_ID', 'asc')
//                ->orderBy('CAT_STATION.IND_ST')
//                ->whereBetween('DATE_CH', [$dateFrom, $dateTo])
//                ->get();

//            $kndaily = new KNDaily($dataForTable, ['dateFrom' => $dateTo, 'dateTo' => $dateTo], $this->categories);
//            $tmp = $kndaily->calculate();
//            $dataForTable = collect($tmp);

            /**
             * Create pagination links
             */
//            $countPages = ceil(count($dataForTable) / PER_PAGE);
//            $paginationLinks = $helper->generateLinksForPagination(url('/kndaily'), $countPages, $currentPage, true);

            /**
             * array with all data for view
             */
            $data = [
                'regions' => $regions->getAllRegions(),
                'stations' => $stations->getAllStation(),
//                'categories' => $this->categories,
//                'dataForTable' => $dataForTable->forPage($currentPage, PER_PAGE),
//                'selectedCategories' => $selectedCategories,
//                'paginationLinks' => $paginationLinks
            ];
        }
        return view('/site.knmonthly.kode_kn_monthly', $data);
    }

    public function getDataKodeKN() {
        var_dump(__METHOD__);
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class Region extends Model
{
    protected $table = 'CAT_OBL';

    protected $primaryKey = "OBL_ID";

    public function station() {
        return $this->hasMany('App\Station', 'OBL_ID');
    }

    public function getAllRegions()
    {
        return DB::table($this->table)->get();
    }
}

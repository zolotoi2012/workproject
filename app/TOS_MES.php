<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class TOS_MES extends Model
{
    protected $table = 'TOS_MES';

    protected $primaryKey = 'IND_ST';

    public function station() {
        return $this->belongsTo('App\Station', 'IND_ST');
    }



}

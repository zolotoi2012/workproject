<?php

use Illuminate\Database\Seeder;

class FirmSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('firms')->insert([
                                    'name' => 'Sony'
                                    ]);
        DB::table('firms')->insert([
                                    'name' => 'Panasonic'
        ]);
        DB::table('firms')->insert([
                                    'name' => 'Samsung'
        ]);
    }
}

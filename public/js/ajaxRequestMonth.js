
$(document).ready(function () {

    var path = location.pathname;
    console.log(path);
    if (path === '/dateformonth') {
        $('input').each(function (index, value) {
            // value.setAttribute('type', 'month');
            var date = new Date();
            var dm = date.getFullYear() + "-" + date.getMonth();
            value.value = dm;
        });
    }

    $('#regions').on('click', function (event) {
        // event.preventDefault();
        var regions_serialize = $('#regions-select').serialize() + "&requestName=selectStation";
        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'), 'Access-Control-Allow-Origin': '*'},
            type: 'POST',
            url: '/',
            data: {data: regions_serialize},
            dataType: 'json',
            success: function (json) {
                $('#stations-select').remove();
                var select = $('<select/>', {
                    id: 'stations-select',
                    class: 'form-control',
                    multiple: 'multiple',
                    name: 'stationName[]',
                    size: '8'
                });

                $.each(json.station, function () {
                    $('<option/>', {
                        val: this.IND_ST,
                        text: this.NAME_ST
                    }).appendTo(select);
                });

                $('.sections-wrapper').append(select);

                // multiSelect('#stations-select');

            }
        });
    });

    $('form').on('submit',function (event) {
        event.preventDefault();
        var $that = $(this),
            fData = $that.serialize() + "&requestName=selectInfoForTable";

        // if (path === '/') var url = $that.attr('action') + '/';
        // else url = $that.attr('action');

        $.ajax({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'), 'Access-Control-Allow-Origin': '*'},
            type: 'POST',
            url: '/dateformonth',
            data: {data: fData},
            async: true,
            dataType: 'html',
            success: function (view)
            {
                $('.table-responsive').remove();
                $('.main-content').html(view);

                $(document).on('click', '.pagination a', function (event) {
                    event.preventDefault();
                    var currentPage = $(this).attr('href').split('page=')[1];
                    getSelectedPage(currentPage, path);
                });

                if (path === '/') getGroup9();
                else if (path === '/warep') getStrings();
                else getStrings();
            }
        });
    });


    if (path === '/') getGroup9();
    else if (path === '/warep') getStrings();
    else getStrings();
});

function getSelectedPage(page, path) {

    var fData = $('form').serialize() + "&requestName=selectInfoForTable" + "&page=" + page;
    $.ajax({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
        type: 'POST',
        url: location.pathname,
        data: {data: fData},
        dataType: 'html'
    }).done(function (view) {

        $('.table-responsive').remove();
        $('.main-content').html(view);

        if (path === '/') getGroup9();
        else if (path === '/warep') getStrings();
    }).fail(function () {
        alert('Page could not be loaded.');
    });
}

function getStrings() {
    $("tr").on('click', function (event) {
        var selectedRow = $(this).children("td")[6].textContent;
        var rows = $($("table").find("tbody tr"));

        var exportURL = $("#export");
        exportURL[0].href = '/export' + '/' + selectedRow;

        setTableHeaders(selectedRow);

        rows.each(function (index, row) {
            if (selectedRow === row.children[6].textContent) {
                $($(row).children("td").splice(9)).css("visibility", "visible");
                $(row).css("backgroundColor", "#808080").css("color", "#ffffff");

            } else {
                $(row).css("backgroundColor", "").css("color", "");
                $($(row).children("td").splice(9)).css("visibility", "hidden")

            }
        });
    });
}


<div class="table-responsive">
    <table class="table table-condensed table-striped">
        <thead>
        <tr>
            @foreach($categories as $category)
                @foreach($selectedCategories as $selectedCategory)
                    @if($category['code_col_name'] == $selectedCategory['code_col_name'])
                        <th>{{  $category['short_col_name'] }}</th>
                    @endif
                @endforeach
            @endforeach
        </tr>
        </thead>
        <tbody>
        @foreach($dataForTable as $item)
            <tr>
                        @php
                        /* $item->NAME_OBL object stdClass !!!*/
                            echo "<td>{$item->NAME_OBL}</td>
                                  <td>{$item->NAME_ST}</td>
                                  <td>{$item->IND_ST}</td>
                                  <td>{$item->YEAR_CH}</td>
                                  <td>{$item->MONTH_CH}</td>"
                        @endphp
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

<ul class="pagination">
    @php
        echo $paginationLinks;
    @endphp
</ul>
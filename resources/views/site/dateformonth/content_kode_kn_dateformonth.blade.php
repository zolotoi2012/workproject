<div class="container-fluid">
    <div class="panel panel-defaul">
        <div class="row">
            <div class="col-md-12">
                <nav class="my-navbar">
                    <ul>
                        <li><a href="{!! url('/')!!}">КC-01(строковий)</a></li>
                        <li><a href="{!! url('/warep') !!}">WAREP</a></li>
                        <li><a href="{!! url('/kndaily') !!}">КC-01(добовий)</a></li>
                        <li><a href="{!! url('/dateformonth') !!}">Дані за місяць</a></li>
                        <li><a href="{!! url('/climat_avg') !!}">CLIMAT(Сер. місячні дані)</a></li>
                    </ul>
                </nav>
            </div>
        </div>

        <div class="panel-body">
            <div class="row">
                {{-- Input fot data --}}
                <div class="col-md-3">
                    <form id="filters" action="{!! url('/dateformonth') !!}" method="POST">
                        {{ csrf_field() }}
                        <div id="dates" class="form-group">
                            <label>Виберіть період:</label>
                            <div class="row">
                                <div class="col-sm-1" style="margin-top: 10px;"> З:</div>
                                <div class="col-sm-11">
                                    <input type='month' class='form-control' name='dateFrom' value="{{ $dateFrom }}" pattern="[0-9]{4}-[0-9]{2}" style="margin-top: 5px;">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-1" style="margin-top: 15px;"> По:</div>
                                <div class="col-sm-11">
                                    <input type='month' class='form-control' name='dateTo' value="{{ $dateTo}}"  pattern="[0-9]{4}-[0-9]{2}" style="margin-top: 10px;">
                                </div>
                            </div>
                        </div>

                        {{-- Regions --}}

                        <div id="regions" class="form-group">
                            <label>Виберіть назву області:</label>
                            <div class="row">
                                <div class="col-sm-12 regions-wrapper">
                                    <select id="regions-select" class="form-control" name="regionName[]" size="6"
                                            multiple>
                                        {{-- Поле с областями --}}
                                        @foreach($regions as $key => $region)
                                            @if($key == 0)
                                                @continue
                                            @else
                                                @if(isset($selectedRegions))
                                                    @if(in_array($region->OBL_ID, $selectedRegions))
                                                        @php
                                                            echo "<option selected value=\"{$region->OBL_ID}\">{$region->NAME_OBL}</option>"?>
                                                        @endphp
                                                    @else
                                                        @php
                                                            echo "<option value=\"{$region->OBL_ID}\">{$region->NAME_OBL}</option>"?>
                                                        @endphp
                                                    @endif

                                                @else
                                                    @php
                                                        echo "<option value=\"{$region->OBL_ID}\">{$region->NAME_OBL}</option>"?>
                                                    @endphp
                                                @endif
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        {{-- Stations --}}

                        <div id="stations" class="form-group">
                            <label>Виберіть назву станції:</label>
                            <div class="row">
                                <div class="col-sm-12 sections-wrapper">
                                    <select id="stations-select" class="form-control" name="stationName[]" size="8"
                                            multiple>

                                        {{-- Поле со станциями --}}
                                       @foreach($stations as $station)
                                            @if(isset($selectedStations))
                                                @if(in_array($station->IND_ST, $selectedStations))
                                                    @php
                                                        echo "<option selected value=\"{$station->IND_ST}\">{$station->NAME_ST}</option>"
                                                    @endphp
                                                @else
                                                    @php
                                                        echo "<option value=\"{$station->IND_ST}\">{$station->NAME_ST}</option>"
                                                    @endphp
                                                @endif
                                            @else
                                                @php
                                                    echo "<option value=\"{$station->IND_ST}\">{$station->NAME_ST}</option>"
                                                @endphp
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <button type="submit" class="btn btn-primary" style="width: 100%; margin-bottom: 10px;">
                                    ОК
                                </button>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="float: right; margin-bottom: 10px;">
                                <a href="{!! url('/export') !!}" id="export" class="btn btn-success" style="width: 100%">Create
                                    Excel</a>
                            </div>
                        </div>
                    </form>

                </div>

                {{-- Data output --}}

                <div class="col-md-9 main-content">
                    <div class="table-responsive">
                        <table id="KN" class="table table-condensed table-striped">
                            <thead>
                            <tr>
                                {{-- Вывод коротких названий данных --}}
                                @foreach($categories as $category)
                                    @foreach($selectedCategories as $selectedCategory)
                                        @if($category['code_col_name'] == $selectedCategory['code_col_name'])
                                            <th>{{  $category['short_col_name'] }}</th>
                                        @endif
                                    @endforeach
                                @endforeach
                            </tr>
                            </thead>
                            <tbody>

                            {{-- Вывод данных в таблицу --}}
                            @foreach($dataForTable as $item)
                                <tr>
                                    @php
                                        $cat = $category['code_col_name'];
                                        echo "<td>{$item->NAME_OBL}</td>
                                              <td>{$item->NAME_ST}</td>
                                              <td>{$item->IND_ST}</td>
                                              <td>{$item->YEAR_CH}</td>
                                              <td>{$item->MONTH_CH}</td>
                                              <td>{$item->T}</td>
                                              <td>{$item->OS}</td>"
                                    @endphp
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <ul class="pagination">
                        @php
                            echo $paginationLinks;
                        @endphp
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
{{--<script type="text/javascript" src="./js/filters.js"></script>--}}
<script type="text/javascript" src="./js/ajaxRequests.js"></script>
